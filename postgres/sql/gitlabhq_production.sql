--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: abuse_reports; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE abuse_reports (
    id integer NOT NULL,
    reporter_id integer,
    user_id integer,
    message text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE abuse_reports OWNER TO gitlab;

--
-- Name: abuse_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE abuse_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE abuse_reports_id_seq OWNER TO gitlab;

--
-- Name: abuse_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE abuse_reports_id_seq OWNED BY abuse_reports.id;


--
-- Name: appearances; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE appearances (
    id integer NOT NULL,
    title character varying,
    description text,
    header_logo character varying,
    logo character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE appearances OWNER TO gitlab;

--
-- Name: appearances_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE appearances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE appearances_id_seq OWNER TO gitlab;

--
-- Name: appearances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE appearances_id_seq OWNED BY appearances.id;


--
-- Name: application_settings; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE application_settings (
    id integer NOT NULL,
    default_projects_limit integer,
    signup_enabled boolean,
    signin_enabled boolean,
    gravatar_enabled boolean,
    sign_in_text text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    home_page_url character varying,
    default_branch_protection integer DEFAULT 2,
    restricted_visibility_levels text,
    version_check_enabled boolean DEFAULT true,
    max_attachment_size integer DEFAULT 10 NOT NULL,
    default_project_visibility integer,
    default_snippet_visibility integer,
    domain_whitelist text,
    user_oauth_applications boolean DEFAULT true,
    after_sign_out_path character varying,
    session_expire_delay integer DEFAULT 10080 NOT NULL,
    import_sources text,
    help_page_text text,
    admin_notification_email character varying,
    shared_runners_enabled boolean DEFAULT true NOT NULL,
    max_artifacts_size integer DEFAULT 100 NOT NULL,
    runners_registration_token character varying,
    require_two_factor_authentication boolean DEFAULT false,
    two_factor_grace_period integer DEFAULT 48,
    metrics_enabled boolean DEFAULT false,
    metrics_host character varying DEFAULT 'localhost'::character varying,
    metrics_pool_size integer DEFAULT 16,
    metrics_timeout integer DEFAULT 10,
    metrics_method_call_threshold integer DEFAULT 10,
    recaptcha_enabled boolean DEFAULT false,
    recaptcha_site_key character varying,
    recaptcha_private_key character varying,
    metrics_port integer DEFAULT 8089,
    akismet_enabled boolean DEFAULT false,
    akismet_api_key character varying,
    metrics_sample_interval integer DEFAULT 15,
    sentry_enabled boolean DEFAULT false,
    sentry_dsn character varying,
    email_author_in_body boolean DEFAULT false,
    default_group_visibility integer,
    repository_checks_enabled boolean DEFAULT false,
    shared_runners_text text,
    metrics_packet_size integer DEFAULT 1,
    disabled_oauth_sign_in_sources text,
    health_check_access_token character varying,
    send_user_confirmation_email boolean DEFAULT false,
    container_registry_token_expire_delay integer DEFAULT 5,
    user_default_external boolean DEFAULT false NOT NULL,
    after_sign_up_text text,
    repository_storage character varying DEFAULT 'default'::character varying,
    enabled_git_access_protocol character varying,
    domain_blacklist_enabled boolean DEFAULT false,
    domain_blacklist text
);


ALTER TABLE application_settings OWNER TO gitlab;

--
-- Name: application_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE application_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE application_settings_id_seq OWNER TO gitlab;

--
-- Name: application_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE application_settings_id_seq OWNED BY application_settings.id;


--
-- Name: audit_events; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE audit_events (
    id integer NOT NULL,
    author_id integer NOT NULL,
    type character varying NOT NULL,
    entity_id integer NOT NULL,
    entity_type character varying NOT NULL,
    details text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE audit_events OWNER TO gitlab;

--
-- Name: audit_events_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE audit_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE audit_events_id_seq OWNER TO gitlab;

--
-- Name: audit_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE audit_events_id_seq OWNED BY audit_events.id;


--
-- Name: award_emoji; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE award_emoji (
    id integer NOT NULL,
    name character varying,
    user_id integer,
    awardable_id integer,
    awardable_type character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE award_emoji OWNER TO gitlab;

--
-- Name: award_emoji_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE award_emoji_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE award_emoji_id_seq OWNER TO gitlab;

--
-- Name: award_emoji_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE award_emoji_id_seq OWNED BY award_emoji.id;


--
-- Name: broadcast_messages; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE broadcast_messages (
    id integer NOT NULL,
    message text NOT NULL,
    starts_at timestamp without time zone,
    ends_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    color character varying,
    font character varying
);


ALTER TABLE broadcast_messages OWNER TO gitlab;

--
-- Name: broadcast_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE broadcast_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE broadcast_messages_id_seq OWNER TO gitlab;

--
-- Name: broadcast_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE broadcast_messages_id_seq OWNED BY broadcast_messages.id;


--
-- Name: ci_application_settings; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_application_settings (
    id integer NOT NULL,
    all_broken_builds boolean,
    add_pusher boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE ci_application_settings OWNER TO gitlab;

--
-- Name: ci_application_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_application_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_application_settings_id_seq OWNER TO gitlab;

--
-- Name: ci_application_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_application_settings_id_seq OWNED BY ci_application_settings.id;


--
-- Name: ci_builds; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_builds (
    id integer NOT NULL,
    project_id integer,
    status character varying,
    finished_at timestamp without time zone,
    trace text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    started_at timestamp without time zone,
    runner_id integer,
    coverage double precision,
    commit_id integer,
    commands text,
    job_id integer,
    name character varying,
    deploy boolean DEFAULT false,
    options text,
    allow_failure boolean DEFAULT false NOT NULL,
    stage character varying,
    trigger_request_id integer,
    stage_idx integer,
    tag boolean,
    ref character varying,
    user_id integer,
    type character varying,
    target_url character varying,
    description character varying,
    artifacts_file text,
    gl_project_id integer,
    artifacts_metadata text,
    erased_by_id integer,
    erased_at timestamp without time zone,
    environment character varying,
    artifacts_expire_at timestamp without time zone,
    artifacts_size integer,
    "when" character varying,
    yaml_variables text
);


ALTER TABLE ci_builds OWNER TO gitlab;

--
-- Name: ci_builds_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_builds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_builds_id_seq OWNER TO gitlab;

--
-- Name: ci_builds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_builds_id_seq OWNED BY ci_builds.id;


--
-- Name: ci_commits; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_commits (
    id integer NOT NULL,
    project_id integer,
    ref character varying,
    sha character varying,
    before_sha character varying,
    push_data text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    tag boolean DEFAULT false,
    yaml_errors text,
    committed_at timestamp without time zone,
    gl_project_id integer,
    status character varying,
    started_at timestamp without time zone,
    finished_at timestamp without time zone,
    duration integer,
    user_id integer
);


ALTER TABLE ci_commits OWNER TO gitlab;

--
-- Name: ci_commits_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_commits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_commits_id_seq OWNER TO gitlab;

--
-- Name: ci_commits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_commits_id_seq OWNED BY ci_commits.id;


--
-- Name: ci_events; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_events (
    id integer NOT NULL,
    project_id integer,
    user_id integer,
    is_admin integer,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE ci_events OWNER TO gitlab;

--
-- Name: ci_events_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_events_id_seq OWNER TO gitlab;

--
-- Name: ci_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_events_id_seq OWNED BY ci_events.id;


--
-- Name: ci_jobs; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_jobs (
    id integer NOT NULL,
    project_id integer NOT NULL,
    commands text,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    name character varying,
    build_branches boolean DEFAULT true NOT NULL,
    build_tags boolean DEFAULT false NOT NULL,
    job_type character varying DEFAULT 'parallel'::character varying,
    refs character varying,
    deleted_at timestamp without time zone
);


ALTER TABLE ci_jobs OWNER TO gitlab;

--
-- Name: ci_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_jobs_id_seq OWNER TO gitlab;

--
-- Name: ci_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_jobs_id_seq OWNED BY ci_jobs.id;


--
-- Name: ci_projects; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_projects (
    id integer NOT NULL,
    name character varying,
    timeout integer DEFAULT 3600 NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    token character varying,
    default_ref character varying,
    path character varying,
    always_build boolean DEFAULT false NOT NULL,
    polling_interval integer,
    public boolean DEFAULT false NOT NULL,
    ssh_url_to_repo character varying,
    gitlab_id integer,
    allow_git_fetch boolean DEFAULT true NOT NULL,
    email_recipients character varying DEFAULT ''::character varying NOT NULL,
    email_add_pusher boolean DEFAULT true NOT NULL,
    email_only_broken_builds boolean DEFAULT true NOT NULL,
    skip_refs character varying,
    coverage_regex character varying,
    shared_runners_enabled boolean DEFAULT false,
    generated_yaml_config text
);


ALTER TABLE ci_projects OWNER TO gitlab;

--
-- Name: ci_projects_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_projects_id_seq OWNER TO gitlab;

--
-- Name: ci_projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_projects_id_seq OWNED BY ci_projects.id;


--
-- Name: ci_runner_projects; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_runner_projects (
    id integer NOT NULL,
    runner_id integer NOT NULL,
    project_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    gl_project_id integer
);


ALTER TABLE ci_runner_projects OWNER TO gitlab;

--
-- Name: ci_runner_projects_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_runner_projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_runner_projects_id_seq OWNER TO gitlab;

--
-- Name: ci_runner_projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_runner_projects_id_seq OWNED BY ci_runner_projects.id;


--
-- Name: ci_runners; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_runners (
    id integer NOT NULL,
    token character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    description character varying,
    contacted_at timestamp without time zone,
    active boolean DEFAULT true NOT NULL,
    is_shared boolean DEFAULT false,
    name character varying,
    version character varying,
    revision character varying,
    platform character varying,
    architecture character varying,
    run_untagged boolean DEFAULT true NOT NULL,
    locked boolean DEFAULT false NOT NULL
);


ALTER TABLE ci_runners OWNER TO gitlab;

--
-- Name: ci_runners_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_runners_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_runners_id_seq OWNER TO gitlab;

--
-- Name: ci_runners_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_runners_id_seq OWNED BY ci_runners.id;


--
-- Name: ci_services; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_services (
    id integer NOT NULL,
    type character varying,
    title character varying,
    project_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    active boolean DEFAULT false NOT NULL,
    properties text
);


ALTER TABLE ci_services OWNER TO gitlab;

--
-- Name: ci_services_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_services_id_seq OWNER TO gitlab;

--
-- Name: ci_services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_services_id_seq OWNED BY ci_services.id;


--
-- Name: ci_sessions; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_sessions (
    id integer NOT NULL,
    session_id character varying NOT NULL,
    data text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE ci_sessions OWNER TO gitlab;

--
-- Name: ci_sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_sessions_id_seq OWNER TO gitlab;

--
-- Name: ci_sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_sessions_id_seq OWNED BY ci_sessions.id;


--
-- Name: ci_taggings; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_taggings (
    id integer NOT NULL,
    tag_id integer,
    taggable_id integer,
    taggable_type character varying,
    tagger_id integer,
    tagger_type character varying,
    context character varying(128),
    created_at timestamp without time zone
);


ALTER TABLE ci_taggings OWNER TO gitlab;

--
-- Name: ci_taggings_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_taggings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_taggings_id_seq OWNER TO gitlab;

--
-- Name: ci_taggings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_taggings_id_seq OWNED BY ci_taggings.id;


--
-- Name: ci_tags; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_tags (
    id integer NOT NULL,
    name character varying,
    taggings_count integer DEFAULT 0
);


ALTER TABLE ci_tags OWNER TO gitlab;

--
-- Name: ci_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_tags_id_seq OWNER TO gitlab;

--
-- Name: ci_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_tags_id_seq OWNED BY ci_tags.id;


--
-- Name: ci_trigger_requests; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_trigger_requests (
    id integer NOT NULL,
    trigger_id integer NOT NULL,
    variables text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    commit_id integer
);


ALTER TABLE ci_trigger_requests OWNER TO gitlab;

--
-- Name: ci_trigger_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_trigger_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_trigger_requests_id_seq OWNER TO gitlab;

--
-- Name: ci_trigger_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_trigger_requests_id_seq OWNED BY ci_trigger_requests.id;


--
-- Name: ci_triggers; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_triggers (
    id integer NOT NULL,
    token character varying,
    project_id integer,
    deleted_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    gl_project_id integer
);


ALTER TABLE ci_triggers OWNER TO gitlab;

--
-- Name: ci_triggers_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_triggers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_triggers_id_seq OWNER TO gitlab;

--
-- Name: ci_triggers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_triggers_id_seq OWNED BY ci_triggers.id;


--
-- Name: ci_variables; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_variables (
    id integer NOT NULL,
    project_id integer,
    key character varying,
    value text,
    encrypted_value text,
    encrypted_value_salt character varying,
    encrypted_value_iv character varying,
    gl_project_id integer
);


ALTER TABLE ci_variables OWNER TO gitlab;

--
-- Name: ci_variables_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_variables_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_variables_id_seq OWNER TO gitlab;

--
-- Name: ci_variables_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_variables_id_seq OWNED BY ci_variables.id;


--
-- Name: ci_web_hooks; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE ci_web_hooks (
    id integer NOT NULL,
    url character varying NOT NULL,
    project_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE ci_web_hooks OWNER TO gitlab;

--
-- Name: ci_web_hooks_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE ci_web_hooks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ci_web_hooks_id_seq OWNER TO gitlab;

--
-- Name: ci_web_hooks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE ci_web_hooks_id_seq OWNED BY ci_web_hooks.id;


--
-- Name: deploy_keys_projects; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE deploy_keys_projects (
    id integer NOT NULL,
    deploy_key_id integer NOT NULL,
    project_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE deploy_keys_projects OWNER TO gitlab;

--
-- Name: deploy_keys_projects_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE deploy_keys_projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE deploy_keys_projects_id_seq OWNER TO gitlab;

--
-- Name: deploy_keys_projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE deploy_keys_projects_id_seq OWNED BY deploy_keys_projects.id;


--
-- Name: deployments; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE deployments (
    id integer NOT NULL,
    iid integer NOT NULL,
    project_id integer NOT NULL,
    environment_id integer NOT NULL,
    ref character varying NOT NULL,
    tag boolean NOT NULL,
    sha character varying NOT NULL,
    user_id integer,
    deployable_id integer,
    deployable_type character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE deployments OWNER TO gitlab;

--
-- Name: deployments_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE deployments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE deployments_id_seq OWNER TO gitlab;

--
-- Name: deployments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE deployments_id_seq OWNED BY deployments.id;


--
-- Name: emails; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE emails (
    id integer NOT NULL,
    user_id integer NOT NULL,
    email character varying NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE emails OWNER TO gitlab;

--
-- Name: emails_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE emails_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE emails_id_seq OWNER TO gitlab;

--
-- Name: emails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE emails_id_seq OWNED BY emails.id;


--
-- Name: environments; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE environments (
    id integer NOT NULL,
    project_id integer,
    name character varying NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE environments OWNER TO gitlab;

--
-- Name: environments_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE environments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE environments_id_seq OWNER TO gitlab;

--
-- Name: environments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE environments_id_seq OWNED BY environments.id;


--
-- Name: events; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE events (
    id integer NOT NULL,
    target_type character varying,
    target_id integer,
    title character varying,
    data text,
    project_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    action integer,
    author_id integer
);


ALTER TABLE events OWNER TO gitlab;

--
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE events_id_seq OWNER TO gitlab;

--
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE events_id_seq OWNED BY events.id;


--
-- Name: forked_project_links; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE forked_project_links (
    id integer NOT NULL,
    forked_to_project_id integer NOT NULL,
    forked_from_project_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE forked_project_links OWNER TO gitlab;

--
-- Name: forked_project_links_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE forked_project_links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE forked_project_links_id_seq OWNER TO gitlab;

--
-- Name: forked_project_links_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE forked_project_links_id_seq OWNED BY forked_project_links.id;


--
-- Name: identities; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE identities (
    id integer NOT NULL,
    extern_uid character varying,
    provider character varying,
    user_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE identities OWNER TO gitlab;

--
-- Name: identities_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE identities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE identities_id_seq OWNER TO gitlab;

--
-- Name: identities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE identities_id_seq OWNED BY identities.id;


--
-- Name: issues; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE issues (
    id integer NOT NULL,
    title character varying,
    assignee_id integer,
    author_id integer,
    project_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    "position" integer DEFAULT 0,
    branch_name character varying,
    description text,
    milestone_id integer,
    state character varying,
    iid integer,
    updated_by_id integer,
    confidential boolean DEFAULT false,
    deleted_at timestamp without time zone,
    due_date date,
    moved_to_id integer
);


ALTER TABLE issues OWNER TO gitlab;

--
-- Name: issues_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE issues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE issues_id_seq OWNER TO gitlab;

--
-- Name: issues_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE issues_id_seq OWNED BY issues.id;


--
-- Name: keys; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE keys (
    id integer NOT NULL,
    user_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    key text,
    title character varying,
    type character varying,
    fingerprint character varying,
    public boolean DEFAULT false NOT NULL
);


ALTER TABLE keys OWNER TO gitlab;

--
-- Name: keys_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE keys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE keys_id_seq OWNER TO gitlab;

--
-- Name: keys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE keys_id_seq OWNED BY keys.id;


--
-- Name: label_links; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE label_links (
    id integer NOT NULL,
    label_id integer,
    target_id integer,
    target_type character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE label_links OWNER TO gitlab;

--
-- Name: label_links_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE label_links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE label_links_id_seq OWNER TO gitlab;

--
-- Name: label_links_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE label_links_id_seq OWNED BY label_links.id;


--
-- Name: labels; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE labels (
    id integer NOT NULL,
    title character varying,
    color character varying,
    project_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    template boolean DEFAULT false,
    description character varying,
    priority integer
);


ALTER TABLE labels OWNER TO gitlab;

--
-- Name: labels_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE labels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE labels_id_seq OWNER TO gitlab;

--
-- Name: labels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE labels_id_seq OWNED BY labels.id;


--
-- Name: lfs_objects; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE lfs_objects (
    id integer NOT NULL,
    oid character varying NOT NULL,
    size bigint NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    file character varying
);


ALTER TABLE lfs_objects OWNER TO gitlab;

--
-- Name: lfs_objects_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE lfs_objects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE lfs_objects_id_seq OWNER TO gitlab;

--
-- Name: lfs_objects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE lfs_objects_id_seq OWNED BY lfs_objects.id;


--
-- Name: lfs_objects_projects; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE lfs_objects_projects (
    id integer NOT NULL,
    lfs_object_id integer NOT NULL,
    project_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE lfs_objects_projects OWNER TO gitlab;

--
-- Name: lfs_objects_projects_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE lfs_objects_projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE lfs_objects_projects_id_seq OWNER TO gitlab;

--
-- Name: lfs_objects_projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE lfs_objects_projects_id_seq OWNED BY lfs_objects_projects.id;


--
-- Name: members; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE members (
    id integer NOT NULL,
    access_level integer NOT NULL,
    source_id integer NOT NULL,
    source_type character varying NOT NULL,
    user_id integer,
    notification_level integer NOT NULL,
    type character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    created_by_id integer,
    invite_email character varying,
    invite_token character varying,
    invite_accepted_at timestamp without time zone,
    requested_at timestamp without time zone
);


ALTER TABLE members OWNER TO gitlab;

--
-- Name: members_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE members_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE members_id_seq OWNER TO gitlab;

--
-- Name: members_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE members_id_seq OWNED BY members.id;


--
-- Name: merge_request_diffs; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE merge_request_diffs (
    id integer NOT NULL,
    state character varying,
    st_commits text,
    st_diffs text,
    merge_request_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    base_commit_sha character varying,
    real_size character varying,
    head_commit_sha character varying,
    start_commit_sha character varying
);


ALTER TABLE merge_request_diffs OWNER TO gitlab;

--
-- Name: merge_request_diffs_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE merge_request_diffs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE merge_request_diffs_id_seq OWNER TO gitlab;

--
-- Name: merge_request_diffs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE merge_request_diffs_id_seq OWNED BY merge_request_diffs.id;


--
-- Name: merge_requests; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE merge_requests (
    id integer NOT NULL,
    target_branch character varying NOT NULL,
    source_branch character varying NOT NULL,
    source_project_id integer NOT NULL,
    author_id integer,
    assignee_id integer,
    title character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    milestone_id integer,
    state character varying,
    merge_status character varying,
    target_project_id integer NOT NULL,
    iid integer,
    description text,
    "position" integer DEFAULT 0,
    locked_at timestamp without time zone,
    updated_by_id integer,
    merge_error character varying,
    merge_params text,
    merge_when_build_succeeds boolean DEFAULT false NOT NULL,
    merge_user_id integer,
    merge_commit_sha character varying,
    deleted_at timestamp without time zone,
    in_progress_merge_commit_sha character varying
);


ALTER TABLE merge_requests OWNER TO gitlab;

--
-- Name: merge_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE merge_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE merge_requests_id_seq OWNER TO gitlab;

--
-- Name: merge_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE merge_requests_id_seq OWNED BY merge_requests.id;


--
-- Name: milestones; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE milestones (
    id integer NOT NULL,
    title character varying NOT NULL,
    project_id integer NOT NULL,
    description text,
    due_date date,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    state character varying,
    iid integer
);


ALTER TABLE milestones OWNER TO gitlab;

--
-- Name: milestones_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE milestones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE milestones_id_seq OWNER TO gitlab;

--
-- Name: milestones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE milestones_id_seq OWNED BY milestones.id;


--
-- Name: namespaces; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE namespaces (
    id integer NOT NULL,
    name character varying NOT NULL,
    path character varying NOT NULL,
    owner_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    type character varying,
    description character varying DEFAULT ''::character varying NOT NULL,
    avatar character varying,
    share_with_group_lock boolean DEFAULT false,
    visibility_level integer DEFAULT 20 NOT NULL,
    request_access_enabled boolean DEFAULT true NOT NULL
);


ALTER TABLE namespaces OWNER TO gitlab;

--
-- Name: namespaces_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE namespaces_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE namespaces_id_seq OWNER TO gitlab;

--
-- Name: namespaces_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE namespaces_id_seq OWNED BY namespaces.id;


--
-- Name: notes; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE notes (
    id integer NOT NULL,
    note text,
    noteable_type character varying,
    author_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    project_id integer,
    attachment character varying,
    line_code character varying,
    commit_id character varying,
    noteable_id integer,
    system boolean DEFAULT false NOT NULL,
    st_diff text,
    updated_by_id integer,
    type character varying,
    "position" text,
    original_position text
);


ALTER TABLE notes OWNER TO gitlab;

--
-- Name: notes_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE notes_id_seq OWNER TO gitlab;

--
-- Name: notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE notes_id_seq OWNED BY notes.id;


--
-- Name: notification_settings; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE notification_settings (
    id integer NOT NULL,
    user_id integer NOT NULL,
    source_id integer,
    source_type character varying,
    level integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    events text
);


ALTER TABLE notification_settings OWNER TO gitlab;

--
-- Name: notification_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE notification_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE notification_settings_id_seq OWNER TO gitlab;

--
-- Name: notification_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE notification_settings_id_seq OWNED BY notification_settings.id;


--
-- Name: oauth_access_grants; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE oauth_access_grants (
    id integer NOT NULL,
    resource_owner_id integer NOT NULL,
    application_id integer NOT NULL,
    token character varying NOT NULL,
    expires_in integer NOT NULL,
    redirect_uri text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    revoked_at timestamp without time zone,
    scopes character varying
);


ALTER TABLE oauth_access_grants OWNER TO gitlab;

--
-- Name: oauth_access_grants_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE oauth_access_grants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE oauth_access_grants_id_seq OWNER TO gitlab;

--
-- Name: oauth_access_grants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE oauth_access_grants_id_seq OWNED BY oauth_access_grants.id;


--
-- Name: oauth_access_tokens; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE oauth_access_tokens (
    id integer NOT NULL,
    resource_owner_id integer,
    application_id integer,
    token character varying NOT NULL,
    refresh_token character varying,
    expires_in integer,
    revoked_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    scopes character varying
);


ALTER TABLE oauth_access_tokens OWNER TO gitlab;

--
-- Name: oauth_access_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE oauth_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE oauth_access_tokens_id_seq OWNER TO gitlab;

--
-- Name: oauth_access_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE oauth_access_tokens_id_seq OWNED BY oauth_access_tokens.id;


--
-- Name: oauth_applications; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE oauth_applications (
    id integer NOT NULL,
    name character varying NOT NULL,
    uid character varying NOT NULL,
    secret character varying NOT NULL,
    redirect_uri text NOT NULL,
    scopes character varying DEFAULT ''::character varying NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    owner_id integer,
    owner_type character varying
);


ALTER TABLE oauth_applications OWNER TO gitlab;

--
-- Name: oauth_applications_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE oauth_applications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE oauth_applications_id_seq OWNER TO gitlab;

--
-- Name: oauth_applications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE oauth_applications_id_seq OWNED BY oauth_applications.id;


--
-- Name: personal_access_tokens; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE personal_access_tokens (
    id integer NOT NULL,
    user_id integer NOT NULL,
    token character varying NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    revoked boolean DEFAULT false,
    expires_at timestamp without time zone
);


ALTER TABLE personal_access_tokens OWNER TO gitlab;

--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE personal_access_tokens_id_seq OWNER TO gitlab;

--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE personal_access_tokens_id_seq OWNED BY personal_access_tokens.id;


--
-- Name: project_group_links; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE project_group_links (
    id integer NOT NULL,
    project_id integer NOT NULL,
    group_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    group_access integer DEFAULT 30 NOT NULL
);


ALTER TABLE project_group_links OWNER TO gitlab;

--
-- Name: project_group_links_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE project_group_links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE project_group_links_id_seq OWNER TO gitlab;

--
-- Name: project_group_links_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE project_group_links_id_seq OWNED BY project_group_links.id;


--
-- Name: project_import_data; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE project_import_data (
    id integer NOT NULL,
    project_id integer,
    data text,
    encrypted_credentials text,
    encrypted_credentials_iv character varying,
    encrypted_credentials_salt character varying
);


ALTER TABLE project_import_data OWNER TO gitlab;

--
-- Name: project_import_data_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE project_import_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE project_import_data_id_seq OWNER TO gitlab;

--
-- Name: project_import_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE project_import_data_id_seq OWNED BY project_import_data.id;


--
-- Name: projects; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE projects (
    id integer NOT NULL,
    name character varying,
    path character varying,
    description text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    creator_id integer,
    issues_enabled boolean DEFAULT true NOT NULL,
    merge_requests_enabled boolean DEFAULT true NOT NULL,
    wiki_enabled boolean DEFAULT true NOT NULL,
    namespace_id integer,
    snippets_enabled boolean DEFAULT true NOT NULL,
    last_activity_at timestamp without time zone,
    import_url character varying,
    visibility_level integer DEFAULT 0 NOT NULL,
    archived boolean DEFAULT false NOT NULL,
    avatar character varying,
    import_status character varying,
    repository_size double precision DEFAULT 0.0,
    star_count integer DEFAULT 0 NOT NULL,
    import_type character varying,
    import_source character varying,
    commit_count integer DEFAULT 0,
    import_error text,
    ci_id integer,
    builds_enabled boolean DEFAULT true NOT NULL,
    shared_runners_enabled boolean DEFAULT true NOT NULL,
    runners_token character varying,
    build_coverage_regex character varying,
    build_allow_git_fetch boolean DEFAULT true NOT NULL,
    build_timeout integer DEFAULT 3600 NOT NULL,
    pending_delete boolean DEFAULT false,
    public_builds boolean DEFAULT true NOT NULL,
    pushes_since_gc integer DEFAULT 0,
    last_repository_check_failed boolean,
    last_repository_check_at timestamp without time zone,
    container_registry_enabled boolean,
    only_allow_merge_if_build_succeeds boolean DEFAULT false NOT NULL,
    has_external_issue_tracker boolean,
    repository_storage character varying DEFAULT 'default'::character varying NOT NULL,
    has_external_wiki boolean,
    request_access_enabled boolean DEFAULT true NOT NULL
);


ALTER TABLE projects OWNER TO gitlab;

--
-- Name: projects_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE projects_id_seq OWNER TO gitlab;

--
-- Name: projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE projects_id_seq OWNED BY projects.id;


--
-- Name: protected_branches; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE protected_branches (
    id integer NOT NULL,
    project_id integer NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    developers_can_push boolean DEFAULT false NOT NULL,
    developers_can_merge boolean DEFAULT false NOT NULL
);


ALTER TABLE protected_branches OWNER TO gitlab;

--
-- Name: protected_branches_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE protected_branches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE protected_branches_id_seq OWNER TO gitlab;

--
-- Name: protected_branches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE protected_branches_id_seq OWNED BY protected_branches.id;


--
-- Name: releases; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE releases (
    id integer NOT NULL,
    tag character varying,
    description text,
    project_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE releases OWNER TO gitlab;

--
-- Name: releases_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE releases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE releases_id_seq OWNER TO gitlab;

--
-- Name: releases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE releases_id_seq OWNED BY releases.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE schema_migrations OWNER TO gitlab;

--
-- Name: sent_notifications; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE sent_notifications (
    id integer NOT NULL,
    project_id integer,
    noteable_id integer,
    noteable_type character varying,
    recipient_id integer,
    commit_id character varying,
    reply_key character varying NOT NULL,
    line_code character varying,
    note_type character varying,
    "position" text
);


ALTER TABLE sent_notifications OWNER TO gitlab;

--
-- Name: sent_notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE sent_notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sent_notifications_id_seq OWNER TO gitlab;

--
-- Name: sent_notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE sent_notifications_id_seq OWNED BY sent_notifications.id;


--
-- Name: services; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE services (
    id integer NOT NULL,
    type character varying,
    title character varying,
    project_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    active boolean DEFAULT false NOT NULL,
    properties text,
    template boolean DEFAULT false,
    push_events boolean DEFAULT true,
    issues_events boolean DEFAULT true,
    merge_requests_events boolean DEFAULT true,
    tag_push_events boolean DEFAULT true,
    note_events boolean DEFAULT true NOT NULL,
    build_events boolean DEFAULT false NOT NULL,
    category character varying DEFAULT 'common'::character varying NOT NULL,
    "default" boolean DEFAULT false,
    wiki_page_events boolean DEFAULT true
);


ALTER TABLE services OWNER TO gitlab;

--
-- Name: services_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE services_id_seq OWNER TO gitlab;

--
-- Name: services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE services_id_seq OWNED BY services.id;


--
-- Name: snippets; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE snippets (
    id integer NOT NULL,
    title character varying,
    content text,
    author_id integer NOT NULL,
    project_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    file_name character varying,
    type character varying,
    visibility_level integer DEFAULT 0 NOT NULL
);


ALTER TABLE snippets OWNER TO gitlab;

--
-- Name: snippets_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE snippets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE snippets_id_seq OWNER TO gitlab;

--
-- Name: snippets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE snippets_id_seq OWNED BY snippets.id;


--
-- Name: spam_logs; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE spam_logs (
    id integer NOT NULL,
    user_id integer,
    source_ip character varying,
    user_agent character varying,
    via_api boolean,
    project_id integer,
    noteable_type character varying,
    title character varying,
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE spam_logs OWNER TO gitlab;

--
-- Name: spam_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE spam_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spam_logs_id_seq OWNER TO gitlab;

--
-- Name: spam_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE spam_logs_id_seq OWNED BY spam_logs.id;


--
-- Name: subscriptions; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE subscriptions (
    id integer NOT NULL,
    user_id integer,
    subscribable_id integer,
    subscribable_type character varying,
    subscribed boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE subscriptions OWNER TO gitlab;

--
-- Name: subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE subscriptions_id_seq OWNER TO gitlab;

--
-- Name: subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE subscriptions_id_seq OWNED BY subscriptions.id;


--
-- Name: taggings; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE taggings (
    id integer NOT NULL,
    tag_id integer,
    taggable_id integer,
    taggable_type character varying,
    tagger_id integer,
    tagger_type character varying,
    context character varying,
    created_at timestamp without time zone
);


ALTER TABLE taggings OWNER TO gitlab;

--
-- Name: taggings_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE taggings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE taggings_id_seq OWNER TO gitlab;

--
-- Name: taggings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE taggings_id_seq OWNED BY taggings.id;


--
-- Name: tags; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE tags (
    id integer NOT NULL,
    name character varying,
    taggings_count integer DEFAULT 0
);


ALTER TABLE tags OWNER TO gitlab;

--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tags_id_seq OWNER TO gitlab;

--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE tags_id_seq OWNED BY tags.id;


--
-- Name: todos; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE todos (
    id integer NOT NULL,
    user_id integer NOT NULL,
    project_id integer NOT NULL,
    target_id integer,
    target_type character varying NOT NULL,
    author_id integer,
    action integer NOT NULL,
    state character varying NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    note_id integer,
    commit_id character varying
);


ALTER TABLE todos OWNER TO gitlab;

--
-- Name: todos_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE todos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE todos_id_seq OWNER TO gitlab;

--
-- Name: todos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE todos_id_seq OWNED BY todos.id;


--
-- Name: u2f_registrations; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE u2f_registrations (
    id integer NOT NULL,
    certificate text,
    key_handle character varying,
    public_key character varying,
    counter integer,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE u2f_registrations OWNER TO gitlab;

--
-- Name: u2f_registrations_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE u2f_registrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE u2f_registrations_id_seq OWNER TO gitlab;

--
-- Name: u2f_registrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE u2f_registrations_id_seq OWNED BY u2f_registrations.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying,
    last_sign_in_ip character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    name character varying,
    admin boolean DEFAULT false NOT NULL,
    projects_limit integer DEFAULT 10,
    skype character varying DEFAULT ''::character varying NOT NULL,
    linkedin character varying DEFAULT ''::character varying NOT NULL,
    twitter character varying DEFAULT ''::character varying NOT NULL,
    authentication_token character varying,
    theme_id integer DEFAULT 1 NOT NULL,
    bio character varying,
    failed_attempts integer DEFAULT 0,
    locked_at timestamp without time zone,
    username character varying,
    can_create_group boolean DEFAULT true NOT NULL,
    can_create_team boolean DEFAULT true NOT NULL,
    state character varying,
    color_scheme_id integer DEFAULT 1 NOT NULL,
    password_expires_at timestamp without time zone,
    created_by_id integer,
    last_credential_check_at timestamp without time zone,
    avatar character varying,
    confirmation_token character varying,
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    unconfirmed_email character varying,
    hide_no_ssh_key boolean DEFAULT false,
    website_url character varying DEFAULT ''::character varying NOT NULL,
    notification_email character varying,
    hide_no_password boolean DEFAULT false,
    password_automatically_set boolean DEFAULT false,
    location character varying,
    encrypted_otp_secret character varying,
    encrypted_otp_secret_iv character varying,
    encrypted_otp_secret_salt character varying,
    otp_required_for_login boolean DEFAULT false NOT NULL,
    otp_backup_codes text,
    public_email character varying DEFAULT ''::character varying NOT NULL,
    dashboard integer DEFAULT 0,
    project_view integer DEFAULT 0,
    consumed_timestep integer,
    layout integer DEFAULT 0,
    hide_project_limit boolean DEFAULT false,
    unlock_token character varying,
    otp_grace_period_started_at timestamp without time zone,
    ldap_email boolean DEFAULT false NOT NULL,
    external boolean DEFAULT false
);


ALTER TABLE users OWNER TO gitlab;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO gitlab;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: users_star_projects; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE users_star_projects (
    id integer NOT NULL,
    project_id integer NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE users_star_projects OWNER TO gitlab;

--
-- Name: users_star_projects_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE users_star_projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_star_projects_id_seq OWNER TO gitlab;

--
-- Name: users_star_projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE users_star_projects_id_seq OWNED BY users_star_projects.id;


--
-- Name: web_hooks; Type: TABLE; Schema: public; Owner: gitlab
--

CREATE TABLE web_hooks (
    id integer NOT NULL,
    url character varying(2000),
    project_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    type character varying DEFAULT 'ProjectHook'::character varying,
    service_id integer,
    push_events boolean DEFAULT true NOT NULL,
    issues_events boolean DEFAULT false NOT NULL,
    merge_requests_events boolean DEFAULT false NOT NULL,
    tag_push_events boolean DEFAULT false,
    note_events boolean DEFAULT false NOT NULL,
    enable_ssl_verification boolean DEFAULT true,
    build_events boolean DEFAULT false NOT NULL,
    wiki_page_events boolean DEFAULT false NOT NULL,
    token character varying
);


ALTER TABLE web_hooks OWNER TO gitlab;

--
-- Name: web_hooks_id_seq; Type: SEQUENCE; Schema: public; Owner: gitlab
--

CREATE SEQUENCE web_hooks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE web_hooks_id_seq OWNER TO gitlab;

--
-- Name: web_hooks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gitlab
--

ALTER SEQUENCE web_hooks_id_seq OWNED BY web_hooks.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY abuse_reports ALTER COLUMN id SET DEFAULT nextval('abuse_reports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY appearances ALTER COLUMN id SET DEFAULT nextval('appearances_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY application_settings ALTER COLUMN id SET DEFAULT nextval('application_settings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY audit_events ALTER COLUMN id SET DEFAULT nextval('audit_events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY award_emoji ALTER COLUMN id SET DEFAULT nextval('award_emoji_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY broadcast_messages ALTER COLUMN id SET DEFAULT nextval('broadcast_messages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_application_settings ALTER COLUMN id SET DEFAULT nextval('ci_application_settings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_builds ALTER COLUMN id SET DEFAULT nextval('ci_builds_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_commits ALTER COLUMN id SET DEFAULT nextval('ci_commits_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_events ALTER COLUMN id SET DEFAULT nextval('ci_events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_jobs ALTER COLUMN id SET DEFAULT nextval('ci_jobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_projects ALTER COLUMN id SET DEFAULT nextval('ci_projects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_runner_projects ALTER COLUMN id SET DEFAULT nextval('ci_runner_projects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_runners ALTER COLUMN id SET DEFAULT nextval('ci_runners_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_services ALTER COLUMN id SET DEFAULT nextval('ci_services_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_sessions ALTER COLUMN id SET DEFAULT nextval('ci_sessions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_taggings ALTER COLUMN id SET DEFAULT nextval('ci_taggings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_tags ALTER COLUMN id SET DEFAULT nextval('ci_tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_trigger_requests ALTER COLUMN id SET DEFAULT nextval('ci_trigger_requests_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_triggers ALTER COLUMN id SET DEFAULT nextval('ci_triggers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_variables ALTER COLUMN id SET DEFAULT nextval('ci_variables_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_web_hooks ALTER COLUMN id SET DEFAULT nextval('ci_web_hooks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY deploy_keys_projects ALTER COLUMN id SET DEFAULT nextval('deploy_keys_projects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY deployments ALTER COLUMN id SET DEFAULT nextval('deployments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY emails ALTER COLUMN id SET DEFAULT nextval('emails_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY environments ALTER COLUMN id SET DEFAULT nextval('environments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY events ALTER COLUMN id SET DEFAULT nextval('events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY forked_project_links ALTER COLUMN id SET DEFAULT nextval('forked_project_links_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY identities ALTER COLUMN id SET DEFAULT nextval('identities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY issues ALTER COLUMN id SET DEFAULT nextval('issues_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY keys ALTER COLUMN id SET DEFAULT nextval('keys_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY label_links ALTER COLUMN id SET DEFAULT nextval('label_links_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY labels ALTER COLUMN id SET DEFAULT nextval('labels_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY lfs_objects ALTER COLUMN id SET DEFAULT nextval('lfs_objects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY lfs_objects_projects ALTER COLUMN id SET DEFAULT nextval('lfs_objects_projects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY members ALTER COLUMN id SET DEFAULT nextval('members_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY merge_request_diffs ALTER COLUMN id SET DEFAULT nextval('merge_request_diffs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY merge_requests ALTER COLUMN id SET DEFAULT nextval('merge_requests_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY milestones ALTER COLUMN id SET DEFAULT nextval('milestones_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY namespaces ALTER COLUMN id SET DEFAULT nextval('namespaces_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY notes ALTER COLUMN id SET DEFAULT nextval('notes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY notification_settings ALTER COLUMN id SET DEFAULT nextval('notification_settings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY oauth_access_grants ALTER COLUMN id SET DEFAULT nextval('oauth_access_grants_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY oauth_access_tokens ALTER COLUMN id SET DEFAULT nextval('oauth_access_tokens_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY oauth_applications ALTER COLUMN id SET DEFAULT nextval('oauth_applications_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('personal_access_tokens_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY project_group_links ALTER COLUMN id SET DEFAULT nextval('project_group_links_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY project_import_data ALTER COLUMN id SET DEFAULT nextval('project_import_data_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY projects ALTER COLUMN id SET DEFAULT nextval('projects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY protected_branches ALTER COLUMN id SET DEFAULT nextval('protected_branches_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY releases ALTER COLUMN id SET DEFAULT nextval('releases_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY sent_notifications ALTER COLUMN id SET DEFAULT nextval('sent_notifications_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY services ALTER COLUMN id SET DEFAULT nextval('services_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY snippets ALTER COLUMN id SET DEFAULT nextval('snippets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY spam_logs ALTER COLUMN id SET DEFAULT nextval('spam_logs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY subscriptions ALTER COLUMN id SET DEFAULT nextval('subscriptions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY taggings ALTER COLUMN id SET DEFAULT nextval('taggings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY tags ALTER COLUMN id SET DEFAULT nextval('tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY todos ALTER COLUMN id SET DEFAULT nextval('todos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY u2f_registrations ALTER COLUMN id SET DEFAULT nextval('u2f_registrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY users_star_projects ALTER COLUMN id SET DEFAULT nextval('users_star_projects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY web_hooks ALTER COLUMN id SET DEFAULT nextval('web_hooks_id_seq'::regclass);


--
-- Data for Name: abuse_reports; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY abuse_reports (id, reporter_id, user_id, message, created_at, updated_at) FROM stdin;
\.


--
-- Name: abuse_reports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('abuse_reports_id_seq', 1, false);


--
-- Data for Name: appearances; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY appearances (id, title, description, header_logo, logo, created_at, updated_at) FROM stdin;
\.


--
-- Name: appearances_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('appearances_id_seq', 1, false);


--
-- Data for Name: application_settings; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY application_settings (id, default_projects_limit, signup_enabled, signin_enabled, gravatar_enabled, sign_in_text, created_at, updated_at, home_page_url, default_branch_protection, restricted_visibility_levels, version_check_enabled, max_attachment_size, default_project_visibility, default_snippet_visibility, domain_whitelist, user_oauth_applications, after_sign_out_path, session_expire_delay, import_sources, help_page_text, admin_notification_email, shared_runners_enabled, max_artifacts_size, runners_registration_token, require_two_factor_authentication, two_factor_grace_period, metrics_enabled, metrics_host, metrics_pool_size, metrics_timeout, metrics_method_call_threshold, recaptcha_enabled, recaptcha_site_key, recaptcha_private_key, metrics_port, akismet_enabled, akismet_api_key, metrics_sample_interval, sentry_enabled, sentry_dsn, email_author_in_body, default_group_visibility, repository_checks_enabled, shared_runners_text, metrics_packet_size, disabled_oauth_sign_in_sources, health_check_access_token, send_user_confirmation_email, container_registry_token_expire_delay, user_default_external, after_sign_up_text, repository_storage, enabled_git_access_protocol, domain_blacklist_enabled, domain_blacklist) FROM stdin;
1	10	t	t	t	\N	2016-08-12 14:41:09.361017	2016-08-12 14:41:09.361017	\N	2	--- []\n	t	10	0	0	\N	t	\N	10080	---\n- github\n- bitbucket\n- gitlab\n- gitorious\n- google_code\n- fogbugz\n- git\n- gitlab_project\n	\N	\N	t	100	qcEpBvmyw5eskNPYb8_Z	f	48	f	localhost	16	10	10	f	\N	\N	8089	f	\N	15	f	\N	f	\N	t	\N	1	\N	LrCAZwJyDnHvbaK6HNaS	f	5	f	\N	default	\N	f	\N
2	10	t	t	t	\N	2016-08-12 14:41:09.389796	2016-08-12 14:41:09.389796	\N	2	--- []\n	t	10	0	0	\N	t	\N	10080	---\n- github\n- bitbucket\n- gitlab\n- gitorious\n- google_code\n- fogbugz\n- git\n- gitlab_project\n	\N	\N	t	100	5Bsneb6vs9h6zcg9b7PA	f	48	f	localhost	16	10	10	f	\N	\N	8089	f	\N	15	f	\N	f	\N	t	\N	1	\N	AYJZskuyqoqizaThy3t5	f	5	f	\N	default	\N	f	\N
3	10	t	t	t	\N	2016-08-12 14:41:09.416963	2016-08-12 14:41:09.416963	\N	2	--- []\n	t	10	0	0	\N	t	\N	10080	---\n- github\n- bitbucket\n- gitlab\n- gitorious\n- google_code\n- fogbugz\n- git\n- gitlab_project\n	\N	\N	t	100	t7yYysg1asKVQhA9NGY6	f	48	f	localhost	16	10	10	f	\N	\N	8089	f	\N	15	f	\N	f	\N	t	\N	1	\N	FtExZJ23WKq5x-1KSs1f	f	5	f	\N	default	\N	f	\N
4	10	t	t	t	\N	2016-08-12 14:41:09.440646	2016-08-12 14:41:09.440646	\N	2	--- []\n	t	10	0	0	\N	t	\N	10080	---\n- github\n- bitbucket\n- gitlab\n- gitorious\n- google_code\n- fogbugz\n- git\n- gitlab_project\n	\N	\N	t	100	SrKtwztBunh7QQmn9zye	f	48	f	localhost	16	10	10	f	\N	\N	8089	f	\N	15	f	\N	f	\N	t	\N	1	\N	kFtdtLfUGBTnxMhqvC8p	f	5	f	\N	default	\N	f	\N
5	10	t	t	t	\N	2016-08-12 14:41:09.534879	2016-08-12 14:41:09.534879	\N	2	--- []\n	t	10	0	0	\N	t	\N	10080	---\n- github\n- bitbucket\n- gitlab\n- gitorious\n- google_code\n- fogbugz\n- git\n- gitlab_project\n	\N	\N	t	100	ruf5nRfPd15DgU4Dq4dJ	f	48	f	localhost	16	10	10	f	\N	\N	8089	f	\N	15	f	\N	f	\N	t	\N	1	\N	QiXyuvjdNtxJDFC6ZyjM	f	5	f	\N	default	\N	f	\N
\.


--
-- Name: application_settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('application_settings_id_seq', 5, true);


--
-- Data for Name: audit_events; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY audit_events (id, author_id, type, entity_id, entity_type, details, created_at, updated_at) FROM stdin;
1	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-12 14:42:09.064621	2016-08-12 14:42:09.064621
2	1	SecurityEvent	1	User	---\n:with: standard\n:target_id: 1\n:target_type: User\n:target_details: Administrator\n	2016-08-12 14:42:14.76238	2016-08-12 14:42:14.76238
5	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-12 14:44:57.227559	2016-08-12 14:44:57.227559
6	1	SecurityEvent	1	User	---\n:with: standard\n:target_id: 1\n:target_type: User\n:target_details: Administrator\n	2016-08-12 14:45:01.369619	2016-08-12 14:45:01.369619
7	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-12 14:45:18.493121	2016-08-12 14:45:18.493121
8	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-12 15:02:10.421003	2016-08-12 15:02:10.421003
9	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-12 15:05:56.596861	2016-08-12 15:05:56.596861
10	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-12 15:26:49.199798	2016-08-12 15:26:49.199798
11	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-12 15:58:28.43725	2016-08-12 15:58:28.43725
12	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-12 16:02:33.979423	2016-08-12 16:02:33.979423
13	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-12 16:34:45.71641	2016-08-12 16:34:45.71641
14	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-12 16:55:33.912592	2016-08-12 16:55:33.912592
15	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-12 17:09:16.122079	2016-08-12 17:09:16.122079
16	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-12 21:07:47.674231	2016-08-12 21:07:47.674231
17	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-13 00:34:01.570517	2016-08-13 00:34:01.570517
18	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-13 01:00:11.00614	2016-08-13 01:00:11.00614
19	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-13 02:54:58.416089	2016-08-13 02:54:58.416089
20	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-16 16:11:01.494378	2016-08-16 16:11:01.494378
21	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-17 14:44:51.593001	2016-08-17 14:44:51.593001
22	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-18 12:58:06.578786	2016-08-18 12:58:06.578786
23	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-18 13:04:24.084262	2016-08-18 13:04:24.084262
24	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-18 13:04:47.503557	2016-08-18 13:04:47.503557
25	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-18 13:05:17.391568	2016-08-18 13:05:17.391568
26	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-18 17:08:55.017478	2016-08-18 17:08:55.017478
27	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-18 18:26:59.437758	2016-08-18 18:26:59.437758
28	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-21 10:09:43.013928	2016-08-21 10:09:43.013928
29	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-21 10:23:38.156328	2016-08-21 10:23:38.156328
30	2	SecurityEvent	2	User	---\n:with: :ldap\n:target_id: 2\n:target_type: User\n:target_details: CA LF User\n	2016-08-21 11:44:32.12129	2016-08-21 11:44:32.12129
\.


--
-- Name: audit_events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('audit_events_id_seq', 30, true);


--
-- Data for Name: award_emoji; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY award_emoji (id, name, user_id, awardable_id, awardable_type, created_at, updated_at) FROM stdin;
\.


--
-- Name: award_emoji_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('award_emoji_id_seq', 1, false);


--
-- Data for Name: broadcast_messages; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY broadcast_messages (id, message, starts_at, ends_at, created_at, updated_at, color, font) FROM stdin;
\.


--
-- Name: broadcast_messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('broadcast_messages_id_seq', 1, false);


--
-- Data for Name: ci_application_settings; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_application_settings (id, all_broken_builds, add_pusher, created_at, updated_at) FROM stdin;
\.


--
-- Name: ci_application_settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_application_settings_id_seq', 1, false);


--
-- Data for Name: ci_builds; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_builds (id, project_id, status, finished_at, trace, created_at, updated_at, started_at, runner_id, coverage, commit_id, commands, job_id, name, deploy, options, allow_failure, stage, trigger_request_id, stage_idx, tag, ref, user_id, type, target_url, description, artifacts_file, gl_project_id, artifacts_metadata, erased_by_id, erased_at, environment, artifacts_expire_at, artifacts_size, "when", yaml_variables) FROM stdin;
\.


--
-- Name: ci_builds_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_builds_id_seq', 1, false);


--
-- Data for Name: ci_commits; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_commits (id, project_id, ref, sha, before_sha, push_data, created_at, updated_at, tag, yaml_errors, committed_at, gl_project_id, status, started_at, finished_at, duration, user_id) FROM stdin;
\.


--
-- Name: ci_commits_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_commits_id_seq', 1, false);


--
-- Data for Name: ci_events; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_events (id, project_id, user_id, is_admin, description, created_at, updated_at) FROM stdin;
\.


--
-- Name: ci_events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_events_id_seq', 1, false);


--
-- Data for Name: ci_jobs; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_jobs (id, project_id, commands, active, created_at, updated_at, name, build_branches, build_tags, job_type, refs, deleted_at) FROM stdin;
\.


--
-- Name: ci_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_jobs_id_seq', 1, false);


--
-- Data for Name: ci_projects; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_projects (id, name, timeout, created_at, updated_at, token, default_ref, path, always_build, polling_interval, public, ssh_url_to_repo, gitlab_id, allow_git_fetch, email_recipients, email_add_pusher, email_only_broken_builds, skip_refs, coverage_regex, shared_runners_enabled, generated_yaml_config) FROM stdin;
\.


--
-- Name: ci_projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_projects_id_seq', 1, false);


--
-- Data for Name: ci_runner_projects; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_runner_projects (id, runner_id, project_id, created_at, updated_at, gl_project_id) FROM stdin;
\.


--
-- Name: ci_runner_projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_runner_projects_id_seq', 1, false);


--
-- Data for Name: ci_runners; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_runners (id, token, created_at, updated_at, description, contacted_at, active, is_shared, name, version, revision, platform, architecture, run_untagged, locked) FROM stdin;
\.


--
-- Name: ci_runners_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_runners_id_seq', 1, false);


--
-- Data for Name: ci_services; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_services (id, type, title, project_id, created_at, updated_at, active, properties) FROM stdin;
\.


--
-- Name: ci_services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_services_id_seq', 1, false);


--
-- Data for Name: ci_sessions; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_sessions (id, session_id, data, created_at, updated_at) FROM stdin;
\.


--
-- Name: ci_sessions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_sessions_id_seq', 1, false);


--
-- Data for Name: ci_taggings; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_taggings (id, tag_id, taggable_id, taggable_type, tagger_id, tagger_type, context, created_at) FROM stdin;
\.


--
-- Name: ci_taggings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_taggings_id_seq', 1, false);


--
-- Data for Name: ci_tags; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_tags (id, name, taggings_count) FROM stdin;
\.


--
-- Name: ci_tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_tags_id_seq', 1, false);


--
-- Data for Name: ci_trigger_requests; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_trigger_requests (id, trigger_id, variables, created_at, updated_at, commit_id) FROM stdin;
\.


--
-- Name: ci_trigger_requests_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_trigger_requests_id_seq', 1, false);


--
-- Data for Name: ci_triggers; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_triggers (id, token, project_id, deleted_at, created_at, updated_at, gl_project_id) FROM stdin;
1	cccd2023c2fd46ceb98c6d47c2e5a5	\N	\N	2016-08-12 16:08:02.530048	2016-08-12 16:08:02.530048	1
2	67b3ab6e839af019915eacb326dc53	\N	2016-08-12 16:11:42.191231	2016-08-12 16:11:39.03986	2016-08-12 16:11:39.03986	1
\.


--
-- Name: ci_triggers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_triggers_id_seq', 2, true);


--
-- Data for Name: ci_variables; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_variables (id, project_id, key, value, encrypted_value, encrypted_value_salt, encrypted_value_iv, gl_project_id) FROM stdin;
\.


--
-- Name: ci_variables_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_variables_id_seq', 1, false);


--
-- Data for Name: ci_web_hooks; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY ci_web_hooks (id, url, project_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: ci_web_hooks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('ci_web_hooks_id_seq', 1, false);


--
-- Data for Name: deploy_keys_projects; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY deploy_keys_projects (id, deploy_key_id, project_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: deploy_keys_projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('deploy_keys_projects_id_seq', 1, false);


--
-- Data for Name: deployments; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY deployments (id, iid, project_id, environment_id, ref, tag, sha, user_id, deployable_id, deployable_type, created_at, updated_at) FROM stdin;
\.


--
-- Name: deployments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('deployments_id_seq', 1, false);


--
-- Data for Name: emails; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY emails (id, user_id, email, created_at, updated_at) FROM stdin;
\.


--
-- Name: emails_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('emails_id_seq', 1, false);


--
-- Data for Name: environments; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY environments (id, project_id, name, created_at, updated_at) FROM stdin;
\.


--
-- Name: environments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('environments_id_seq', 1, false);


--
-- Data for Name: events; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY events (id, target_type, target_id, title, data, project_id, created_at, updated_at, action, author_id) FROM stdin;
1	\N	\N	\N	\N	1	2016-08-12 15:28:33.821809	2016-08-18 17:13:10.901966	1	2
2	\N	\N	\N	---\n:object_kind: push\n:event_name: push\n:before: '0000000000000000000000000000000000000000'\n:after: 3451ce944c3bc6ba77b1c6239adf14be222b0ac4\n:ref: refs/heads/master\n:checkout_sha: 3451ce944c3bc6ba77b1c6239adf14be222b0ac4\n:message: \n:user_id: 2\n:user_name: CA LF User\n:user_email: calfuser@capgemini.com\n:user_avatar: http://www.gravatar.com/avatar/73ed9671476a835caadba257b9a1dc62?s=80&d=identicon\n:project_id: 1\n:project:\n  :name: pic-project\n  :description: PIC\n  :web_url: http://cfe0202533ab/calfuser/pic-project\n  :avatar_url: \n  :git_ssh_url: git@cfe0202533ab:calfuser/pic-project.git\n  :git_http_url: http://cfe0202533ab/calfuser/pic-project.git\n  :namespace: calfuser\n  :visibility_level: 10\n  :path_with_namespace: calfuser/pic-project\n  :default_branch: master\n  :homepage: http://cfe0202533ab/calfuser/pic-project\n  :url: git@cfe0202533ab:calfuser/pic-project.git\n  :ssh_url: git@cfe0202533ab:calfuser/pic-project.git\n  :http_url: http://cfe0202533ab/calfuser/pic-project.git\n:commits:\n- :id: 3451ce944c3bc6ba77b1c6239adf14be222b0ac4\n  :message: |\n    Initialized project\n  :timestamp: '2016-08-12T17:45:34+02:00'\n  :url: http://cfe0202533ab/calfuser/pic-project/commit/3451ce944c3bc6ba77b1c6239adf14be222b0ac4\n  :author:\n    :name: U-CORP\\bgohier\n    :email: bgohier@LFR002601.corp.capgemini.com\n  :added:\n  - README.md\n  :modified: []\n  :removed: []\n:total_commits_count: 1\n:repository:\n  :name: pic-project\n  :url: git@cfe0202533ab:calfuser/pic-project.git\n  :description: PIC\n  :homepage: http://cfe0202533ab/calfuser/pic-project\n  :git_http_url: http://cfe0202533ab/calfuser/pic-project.git\n  :git_ssh_url: git@cfe0202533ab:calfuser/pic-project.git\n  :visibility_level: 10\n	1	2016-08-12 15:45:41.599029	2016-08-18 17:13:10.901966	5	2
3	\N	\N	\N	---\n:object_kind: push\n:event_name: push\n:before: 3451ce944c3bc6ba77b1c6239adf14be222b0ac4\n:after: 9aa7f320b21a049984ad376b16937ba8dac10844\n:ref: refs/heads/master\n:checkout_sha: 9aa7f320b21a049984ad376b16937ba8dac10844\n:message: \n:user_id: 2\n:user_name: CA LF User\n:user_email: calfuser@capgemini.com\n:user_avatar: http://www.gravatar.com/avatar/73ed9671476a835caadba257b9a1dc62?s=80&d=identicon\n:project_id: 1\n:project:\n  :name: pic-project\n  :description: PIC\n  :web_url: http://cfe0202533ab/calfuser/pic-project\n  :avatar_url: \n  :git_ssh_url: git@cfe0202533ab:calfuser/pic-project.git\n  :git_http_url: http://cfe0202533ab/calfuser/pic-project.git\n  :namespace: calfuser\n  :visibility_level: 10\n  :path_with_namespace: calfuser/pic-project\n  :default_branch: master\n  :homepage: http://cfe0202533ab/calfuser/pic-project\n  :url: git@cfe0202533ab:calfuser/pic-project.git\n  :ssh_url: git@cfe0202533ab:calfuser/pic-project.git\n  :http_url: http://cfe0202533ab/calfuser/pic-project.git\n:commits:\n- :id: 9aa7f320b21a049984ad376b16937ba8dac10844\n  :message: |\n    Added pom.xml\n  :timestamp: '2016-08-12T17:53:30+02:00'\n  :url: http://cfe0202533ab/calfuser/pic-project/commit/9aa7f320b21a049984ad376b16937ba8dac10844\n  :author:\n    :name: Brian GOHIER\n    :email: brian.gohier@capgemini.com\n  :added:\n  - pom.xml\n  :modified: []\n  :removed: []\n:total_commits_count: 1\n:repository:\n  :name: pic-project\n  :url: git@cfe0202533ab:calfuser/pic-project.git\n  :description: PIC\n  :homepage: http://cfe0202533ab/calfuser/pic-project\n  :git_http_url: http://cfe0202533ab/calfuser/pic-project.git\n  :git_ssh_url: git@cfe0202533ab:calfuser/pic-project.git\n  :visibility_level: 10\n	1	2016-08-12 15:53:38.328066	2016-08-18 17:13:10.901966	5	2
4	\N	\N	\N	---\n:object_kind: push\n:event_name: push\n:before: '0000000000000000000000000000000000000000'\n:after: 9aa7f320b21a049984ad376b16937ba8dac10844\n:ref: refs/heads/master\n:checkout_sha: 9aa7f320b21a049984ad376b16937ba8dac10844\n:message: \n:user_id: 2\n:user_name: CA LF User\n:user_email: calfuser@capgemini.com\n:user_avatar: http://www.gravatar.com/avatar/73ed9671476a835caadba257b9a1dc62?s=80&d=identicon\n:project_id: 1\n:project:\n  :name: pic-project\n  :description: PIC\n  :web_url: http://45c9a90f54e0/calfuser/pic-project\n  :avatar_url: \n  :git_ssh_url: git@45c9a90f54e0:calfuser/pic-project.git\n  :git_http_url: http://45c9a90f54e0/calfuser/pic-project.git\n  :namespace: calfuser\n  :visibility_level: 0\n  :path_with_namespace: calfuser/pic-project\n  :default_branch: master\n  :homepage: http://45c9a90f54e0/calfuser/pic-project\n  :url: git@45c9a90f54e0:calfuser/pic-project.git\n  :ssh_url: git@45c9a90f54e0:calfuser/pic-project.git\n  :http_url: http://45c9a90f54e0/calfuser/pic-project.git\n:commits:\n- :id: 9aa7f320b21a049984ad376b16937ba8dac10844\n  :message: |\n    Added pom.xml\n  :timestamp: '2016-08-12T17:53:30+02:00'\n  :url: http://45c9a90f54e0/calfuser/pic-project/commit/9aa7f320b21a049984ad376b16937ba8dac10844\n  :author:\n    :name: Brian GOHIER\n    :email: brian.gohier@capgemini.com\n  :added:\n  - pom.xml\n  :modified: []\n  :removed: []\n- :id: 3451ce944c3bc6ba77b1c6239adf14be222b0ac4\n  :message: |\n    Initialized project\n  :timestamp: '2016-08-12T17:45:34+02:00'\n  :url: http://45c9a90f54e0/calfuser/pic-project/commit/3451ce944c3bc6ba77b1c6239adf14be222b0ac4\n  :author:\n    :name: U-CORP\\bgohier\n    :email: bgohier@LFR002601.corp.capgemini.com\n  :added:\n  - README.md\n  :modified: []\n  :removed: []\n:total_commits_count: 2\n:repository:\n  :name: pic-project\n  :url: git@45c9a90f54e0:calfuser/pic-project.git\n  :description: PIC\n  :homepage: http://45c9a90f54e0/calfuser/pic-project\n  :git_http_url: http://45c9a90f54e0/calfuser/pic-project.git\n  :git_ssh_url: git@45c9a90f54e0:calfuser/pic-project.git\n  :visibility_level: 0\n	1	2016-08-12 16:05:45.478663	2016-08-18 17:13:10.901966	5	2
5	\N	\N	\N	---\n:object_kind: push\n:event_name: push\n:before: 9aa7f320b21a049984ad376b16937ba8dac10844\n:after: 8562739cee0c3bf3066a9208dc1622c17360fbd7\n:ref: refs/heads/master\n:checkout_sha: 8562739cee0c3bf3066a9208dc1622c17360fbd7\n:message: \n:user_id: 2\n:user_name: CA LF User\n:user_email: calfuser@capgemini.com\n:user_avatar: http://www.gravatar.com/avatar/73ed9671476a835caadba257b9a1dc62?s=80&d=identicon\n:project_id: 1\n:project:\n  :name: pic-project\n  :description: PIC\n  :web_url: http://45c9a90f54e0/calfuser/pic-project\n  :avatar_url: \n  :git_ssh_url: git@45c9a90f54e0:calfuser/pic-project.git\n  :git_http_url: http://45c9a90f54e0/calfuser/pic-project.git\n  :namespace: calfuser\n  :visibility_level: 0\n  :path_with_namespace: calfuser/pic-project\n  :default_branch: master\n  :homepage: http://45c9a90f54e0/calfuser/pic-project\n  :url: git@45c9a90f54e0:calfuser/pic-project.git\n  :ssh_url: git@45c9a90f54e0:calfuser/pic-project.git\n  :http_url: http://45c9a90f54e0/calfuser/pic-project.git\n:commits:\n- :id: 8562739cee0c3bf3066a9208dc1622c17360fbd7\n  :message: |\n    Test trigger\n  :timestamp: '2016-08-12T18:11:02+02:00'\n  :url: http://45c9a90f54e0/calfuser/pic-project/commit/8562739cee0c3bf3066a9208dc1622c17360fbd7\n  :author:\n    :name: Brian GOHIER\n    :email: brian.gohier@capgemini.com\n  :added: []\n  :modified:\n  - pom.xml\n  :removed: []\n:total_commits_count: 1\n:repository:\n  :name: pic-project\n  :url: git@45c9a90f54e0:calfuser/pic-project.git\n  :description: PIC\n  :homepage: http://45c9a90f54e0/calfuser/pic-project\n  :git_http_url: http://45c9a90f54e0/calfuser/pic-project.git\n  :git_ssh_url: git@45c9a90f54e0:calfuser/pic-project.git\n  :visibility_level: 0\n	1	2016-08-12 16:11:09.408517	2016-08-18 17:13:10.901966	5	2
6	\N	\N	\N	---\n:object_kind: push\n:event_name: push\n:before: '0000000000000000000000000000000000000000'\n:after: 94283624bee155d98362a9989fc3d7aac1d7abb6\n:ref: refs/heads/master\n:checkout_sha: 94283624bee155d98362a9989fc3d7aac1d7abb6\n:message: \n:user_id: 2\n:user_name: CA LF User\n:user_email: calfuser@capgemini.com\n:user_avatar: http://www.gravatar.com/avatar/73ed9671476a835caadba257b9a1dc62?s=80&d=identicon\n:project_id: 1\n:project:\n  :name: pic-project\n  :description: PIC\n  :web_url: http://localhost/gitlab/calfuser/pic-project\n  :avatar_url: \n  :git_ssh_url: git@localhost:calfuser/pic-project.git\n  :git_http_url: http://localhost/gitlab/calfuser/pic-project.git\n  :namespace: calfuser\n  :visibility_level: 10\n  :path_with_namespace: calfuser/pic-project\n  :default_branch: master\n  :homepage: http://localhost/gitlab/calfuser/pic-project\n  :url: git@localhost:calfuser/pic-project.git\n  :ssh_url: git@localhost:calfuser/pic-project.git\n  :http_url: http://localhost/gitlab/calfuser/pic-project.git\n:commits:\n- :id: 94283624bee155d98362a9989fc3d7aac1d7abb6\n  :message: |\n    Reinitialized\n  :timestamp: '2016-08-12T19:00:47+02:00'\n  :url: http://localhost/gitlab/calfuser/pic-project/commit/94283624bee155d98362a9989fc3d7aac1d7abb6\n  :author:\n    :name: Brian GOHIER\n    :email: brian.gohier@capgemini.com\n  :added:\n  - README.md\n  - pom.xml\n  :modified: []\n  :removed: []\n:total_commits_count: 1\n:repository:\n  :name: pic-project\n  :url: git@localhost:calfuser/pic-project.git\n  :description: PIC\n  :homepage: http://localhost/gitlab/calfuser/pic-project\n  :git_http_url: http://localhost/gitlab/calfuser/pic-project.git\n  :git_ssh_url: git@localhost:calfuser/pic-project.git\n  :visibility_level: 10\n	1	2016-08-12 17:00:55.303103	2016-08-18 17:13:10.901966	5	2
7	\N	\N	\N	---\n:object_kind: push\n:event_name: push\n:before: '0000000000000000000000000000000000000000'\n:after: 4069d2fe9e038723e5023ade3184be063177d34d\n:ref: refs/heads/master\n:checkout_sha: 4069d2fe9e038723e5023ade3184be063177d34d\n:message: \n:user_id: 2\n:user_name: CA LF User\n:user_email: calfuser@capgemini.com\n:user_avatar: http://www.gravatar.com/avatar/73ed9671476a835caadba257b9a1dc62?s=80&d=identicon\n:project_id: 1\n:project:\n  :name: pic-project\n  :description: PIC\n  :web_url: http://localhost/gitlab/calfuser/pic-project\n  :avatar_url: \n  :git_ssh_url: git@localhost:calfuser/pic-project.git\n  :git_http_url: http://localhost/gitlab/calfuser/pic-project.git\n  :namespace: calfuser\n  :visibility_level: 10\n  :path_with_namespace: calfuser/pic-project\n  :default_branch: master\n  :homepage: http://localhost/gitlab/calfuser/pic-project\n  :url: git@localhost:calfuser/pic-project.git\n  :ssh_url: git@localhost:calfuser/pic-project.git\n  :http_url: http://localhost/gitlab/calfuser/pic-project.git\n:commits:\n- :id: 4069d2fe9e038723e5023ade3184be063177d34d\n  :message: |\n    Reinitialized\n  :timestamp: '2016-08-12T19:16:38+02:00'\n  :url: http://localhost/gitlab/calfuser/pic-project/commit/4069d2fe9e038723e5023ade3184be063177d34d\n  :author:\n    :name: Brian GOHIER\n    :email: brian.gohier@capgemini.com\n  :added:\n  - README.md\n  - pom.xml\n  :modified: []\n  :removed: []\n:total_commits_count: 1\n:repository:\n  :name: pic-project\n  :url: git@localhost:calfuser/pic-project.git\n  :description: PIC\n  :homepage: http://localhost/gitlab/calfuser/pic-project\n  :git_http_url: http://localhost/gitlab/calfuser/pic-project.git\n  :git_ssh_url: git@localhost:calfuser/pic-project.git\n  :visibility_level: 10\n	1	2016-08-12 17:16:43.360299	2016-08-18 17:13:10.901966	5	2
\.


--
-- Name: events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('events_id_seq', 7, true);


--
-- Data for Name: forked_project_links; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY forked_project_links (id, forked_to_project_id, forked_from_project_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: forked_project_links_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('forked_project_links_id_seq', 1, false);


--
-- Data for Name: identities; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY identities (id, extern_uid, provider, user_id, created_at, updated_at) FROM stdin;
1	uid=calfuser,ou=users,dc=capgemini,dc=com	ldapmain	2	2016-08-12 14:42:08.972723	2016-08-12 14:42:08.972723
\.


--
-- Name: identities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('identities_id_seq', 1, true);


--
-- Data for Name: issues; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY issues (id, title, assignee_id, author_id, project_id, created_at, updated_at, "position", branch_name, description, milestone_id, state, iid, updated_by_id, confidential, deleted_at, due_date, moved_to_id) FROM stdin;
\.


--
-- Name: issues_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('issues_id_seq', 1, false);


--
-- Data for Name: keys; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY keys (id, user_id, created_at, updated_at, key, title, type, fingerprint, public) FROM stdin;
1	2	2016-08-12 15:29:37.514319	2016-08-12 15:29:37.514319	ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDNqwR1Jn4NsqhpRLKRFQtBam4Bm2SMZWYiZgTrXHa45FaRZrgTgVSfiUbXITy8mJr84I8qR6K2Tl90QnrsU8o1p5vcx0ivaxVXb2ry2jq3AFOaDuQAzauuLP7dg3yX+AanrwhMNlOOF3ARA5+89hsitV5us4f0hBep++5k+HA6+crU9EE208LasnzWwpqrlFu8C61ykfwU+oXxg2zptD19Xr+EV4Oum+JTZD/rMuFd3ol1igM1W7KgFW9MkYnVB7TRTwbem/x38+P1/6jQZJUWXBEaTmOyCykqE/ZjjundYJwBikk0orX56aJ+dzyrq5dNIbTS7vhzT085wkEDC6Rt calfuser@capgemini.com	SSH_KEY	\N	1e:2b:e0:87:dd:f7:bc:e4:c4:5c:bc:1f:9b:16:8e:e7	f
2	\N	2016-08-21 11:34:05.778812	2016-08-21 11:34:05.778812	ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDJF+pjrFgGvVXcs1YtLz9JcCF8helaErCZ4V0/q+3l8wMBPjweVTAZQr7QDFcSwZk02EQQFxh5XWL+bnObcCiTO2E+tLpO/gwQ8A4FAyG+Jue9HnGB69BMuPeQYrdIcAarIbqr3mW4TX2LrTszt3/aFieeYW1w2vZWTFHpFewomh8gux2LEWW0dPZjnh4ucri4goyps/Q4/hgTe3FWavlflhGCu68GTBabmD4e4P0sDl3wCMeHJOCzgywyxZ0JLlhxN08jVuKGVj3T+QquYgxVPRsgW8oIXBas5A2oyc9IIS2pjukoey0ORgWjWdOHU0e65CReOcmsfvzBVu6GwsK1 calfuser@capgemini.com	DEPLOY_KEY	DeployKey	7e:f9:c9:dd:b0:02:a2:4d:c1:bb:31:25:4d:1b:b4:22	t
\.


--
-- Name: keys_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('keys_id_seq', 2, true);


--
-- Data for Name: label_links; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY label_links (id, label_id, target_id, target_type, created_at, updated_at) FROM stdin;
\.


--
-- Name: label_links_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('label_links_id_seq', 1, false);


--
-- Data for Name: labels; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY labels (id, title, color, project_id, created_at, updated_at, template, description, priority) FROM stdin;
\.


--
-- Name: labels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('labels_id_seq', 1, false);


--
-- Data for Name: lfs_objects; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY lfs_objects (id, oid, size, created_at, updated_at, file) FROM stdin;
\.


--
-- Name: lfs_objects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('lfs_objects_id_seq', 1, false);


--
-- Data for Name: lfs_objects_projects; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY lfs_objects_projects (id, lfs_object_id, project_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: lfs_objects_projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('lfs_objects_projects_id_seq', 1, false);


--
-- Data for Name: members; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY members (id, access_level, source_id, source_type, user_id, notification_level, type, created_at, updated_at, created_by_id, invite_email, invite_token, invite_accepted_at, requested_at) FROM stdin;
1	40	1	Project	2	3	ProjectMember	2016-08-12 15:28:33.885669	2016-08-12 15:28:33.885669	2	\N	\N	\N	\N
\.


--
-- Name: members_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('members_id_seq', 1, true);


--
-- Data for Name: merge_request_diffs; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY merge_request_diffs (id, state, st_commits, st_diffs, merge_request_id, created_at, updated_at, base_commit_sha, real_size, head_commit_sha, start_commit_sha) FROM stdin;
\.


--
-- Name: merge_request_diffs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('merge_request_diffs_id_seq', 1, false);


--
-- Data for Name: merge_requests; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY merge_requests (id, target_branch, source_branch, source_project_id, author_id, assignee_id, title, created_at, updated_at, milestone_id, state, merge_status, target_project_id, iid, description, "position", locked_at, updated_by_id, merge_error, merge_params, merge_when_build_succeeds, merge_user_id, merge_commit_sha, deleted_at, in_progress_merge_commit_sha) FROM stdin;
\.


--
-- Name: merge_requests_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('merge_requests_id_seq', 1, false);


--
-- Data for Name: milestones; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY milestones (id, title, project_id, description, due_date, created_at, updated_at, state, iid) FROM stdin;
\.


--
-- Name: milestones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('milestones_id_seq', 1, false);


--
-- Data for Name: namespaces; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY namespaces (id, name, path, owner_id, created_at, updated_at, type, description, avatar, share_with_group_lock, visibility_level, request_access_enabled) FROM stdin;
2	calfuser	calfuser	2	2016-08-12 14:42:08.995657	2016-08-12 14:42:08.995657	\N		\N	f	20	t
\.


--
-- Name: namespaces_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('namespaces_id_seq', 2, true);


--
-- Data for Name: notes; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY notes (id, note, noteable_type, author_id, created_at, updated_at, project_id, attachment, line_code, commit_id, noteable_id, system, st_diff, updated_by_id, type, "position", original_position) FROM stdin;
\.


--
-- Name: notes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('notes_id_seq', 1, false);


--
-- Data for Name: notification_settings; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY notification_settings (id, user_id, source_id, source_type, level, created_at, updated_at, events) FROM stdin;
1	2	1	Project	3	2016-08-12 15:28:33.910687	2016-08-12 15:28:33.910687	{"new_note":false,"new_issue":false,"reopen_issue":false,"close_issue":false,"reassign_issue":false,"new_merge_request":false,"reopen_merge_request":false,"close_merge_request":false,"reassign_merge_request":false,"merge_merge_request":false}
\.


--
-- Name: notification_settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('notification_settings_id_seq', 1, true);


--
-- Data for Name: oauth_access_grants; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY oauth_access_grants (id, resource_owner_id, application_id, token, expires_in, redirect_uri, created_at, revoked_at, scopes) FROM stdin;
\.


--
-- Name: oauth_access_grants_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('oauth_access_grants_id_seq', 1, false);


--
-- Data for Name: oauth_access_tokens; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY oauth_access_tokens (id, resource_owner_id, application_id, token, refresh_token, expires_in, revoked_at, created_at, scopes) FROM stdin;
\.


--
-- Name: oauth_access_tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('oauth_access_tokens_id_seq', 1, false);


--
-- Data for Name: oauth_applications; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY oauth_applications (id, name, uid, secret, redirect_uri, scopes, created_at, updated_at, owner_id, owner_type) FROM stdin;
\.


--
-- Name: oauth_applications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('oauth_applications_id_seq', 1, false);


--
-- Data for Name: personal_access_tokens; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY personal_access_tokens (id, user_id, token, name, created_at, updated_at, revoked, expires_at) FROM stdin;
1	2	MNEwJ7X1BAL_zgn5TAbx	gitlab_token	2016-08-12 23:07:13.746786	2016-08-12 23:07:13.746786	f	2018-08-01 00:00:00
\.


--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('personal_access_tokens_id_seq', 1, true);


--
-- Data for Name: project_group_links; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY project_group_links (id, project_id, group_id, created_at, updated_at, group_access) FROM stdin;
\.


--
-- Name: project_group_links_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('project_group_links_id_seq', 1, false);


--
-- Data for Name: project_import_data; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY project_import_data (id, project_id, data, encrypted_credentials, encrypted_credentials_iv, encrypted_credentials_salt) FROM stdin;
\.


--
-- Name: project_import_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('project_import_data_id_seq', 1, false);


--
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY projects (id, name, path, description, created_at, updated_at, creator_id, issues_enabled, merge_requests_enabled, wiki_enabled, namespace_id, snippets_enabled, last_activity_at, import_url, visibility_level, archived, avatar, import_status, repository_size, star_count, import_type, import_source, commit_count, import_error, ci_id, builds_enabled, shared_runners_enabled, runners_token, build_coverage_regex, build_allow_git_fetch, build_timeout, pending_delete, public_builds, pushes_since_gc, last_repository_check_failed, last_repository_check_at, container_registry_enabled, only_allow_merge_if_build_succeeds, has_external_issue_tracker, repository_storage, has_external_wiki, request_access_enabled) FROM stdin;
1	pic-project	pic-project	PIC	2016-08-12 15:28:31.698206	2016-08-12 17:00:55.415261	2	t	t	t	2	f	2016-08-12 17:16:43.360299	\N	10	f	\N	none	0.0800000000000000017	0	\N	\N	1	\N	\N	t	t	MsGoJwQNuwJdcHhxi8Gy	\N	t	3600	f	t	6	f	2016-08-12 23:17:49.317364	t	f	f	default	f	t
\.


--
-- Name: projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('projects_id_seq', 1, true);


--
-- Data for Name: protected_branches; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY protected_branches (id, project_id, name, created_at, updated_at, developers_can_push, developers_can_merge) FROM stdin;
1	1	master	2016-08-12 15:45:41.559047	2016-08-12 15:45:41.559047	f	f
2	1	master	2016-08-12 16:05:45.429527	2016-08-12 16:05:45.429527	f	f
3	1	master	2016-08-12 17:00:55.248863	2016-08-12 17:00:55.248863	f	f
4	1	master	2016-08-12 17:16:43.229525	2016-08-12 17:16:43.229525	f	f
\.


--
-- Name: protected_branches_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('protected_branches_id_seq', 4, true);


--
-- Data for Name: releases; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY releases (id, tag, description, project_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: releases_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('releases_id_seq', 1, false);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY schema_migrations (version) FROM stdin;
20160804150737
20121220064453
20130102143055
20130110172407
20130123114545
20130125090214
20130131070232
20130206084024
20130207104426
20130211085435
20130214154045
20130218140952
20130218141038
20130218141117
20130218141258
20130218141327
20130218141344
20130218141444
20130218141507
20130218141536
20130218141554
20130220124204
20130220125544
20130220125545
20130220133245
20130304104623
20130304104740
20130304105317
20130315124931
20130318212250
20130319214458
20130323174317
20130324151736
20130324172327
20130324203535
20130325173941
20130326142630
20130403003950
20130404164628
20130410175022
20130419190306
20130506085413
20130506090604
20130506095501
20130522141856
20130528184641
20130611210815
20130613165816
20130613173246
20130614132337
20130617095603
20130621195223
20130622115340
20130624162710
20130711063759
20130804151314
20130809124851
20130812143708
20130819182730
20130820102832
20130821090530
20130821090531
20130909132950
20130926081215
20131005191208
20131009115346
20131106151520
20131112114325
20140122114406
20131112220935
20131129154016
20131130165425
20131202192556
20131214224427
20131217102743
20140116231608
20140122112253
20140122122549
20140125162722
20140127170938
20140209025651
20140214102325
20140304005354
20140305193308
20140312145357
20140313092127
20140407135544
20140414131055
20140415124820
20140416074002
20140416185734
20140428105831
20140502115131
20140502125220
20140611135229
20140625115202
20140729134820
20140729140420
20140729145339
20140729152420
20140730111702
20140903115954
20140907220153
20140914113604
20140914145549
20140914173417
20141006143943
20141007100818
20141118150935
20141121133009
20141121161704
20141205134006
20141216155758
20141217125223
20141223135007
20141226080412
20150108073740
20150116234544
20150116234545
20150125163100
20150205211843
20150206181414
20150206222854
20150209222013
20150211172122
20150211174341
20150213104043
20150213114800
20150213121042
20150217123345
20150219004514
20150223022001
20150225065047
20150301014758
20150306023106
20150306023112
20150313012111
20150310194358
20150320234437
20150324155957
20150327122227
20150327150017
20150327223628
20150328132231
20150331183602
20150406133311
20150411000035
20150411180045
20150413192223
20150417121913
20150417122318
20150421120000
20150423033240
20150425164646
20150425164647
20150425164648
20150425164649
20150425164650
20150425164651
20150425173433
20150429002313
20150502064022
20150509180749
20150516060434
20150529111607
20150529150354
20150609141121
20150610065936
20150620233230
20150713160110
20150717130904
20150730122406
20150806104937
20150812080800
20150814065925
20150817163600
20150818213832
20150824002011
20150826001931
20150902001023
20150914215247
20150915001905
20150916000405
20150916114643
20150916145038
20150918084513
20150918161719
20150920010715
20150920161119
20150924125150
20150924125436
20150930001110
20150930095736
20150930110012
20151002112914
20151002121400
20151002122929
20151002122943
20151005075649
20151005150751
20151005162154
20151007120511
20151008110232
20151008123042
20151008130321
20151008143519
20151012173029
20151013092124
20151016131433
20151016195451
20151016195706
20151019111551
20151019111703
20151020145526
20151020173516
20151020173906
20151023112551
20151023144219
20151026182941
20151028152939
20151103001141
20151103133339
20151103134857
20151103134958
20151104105513
20151105094515
20151106000015
20151109100728
20151109134526
20151109134916
20151110125604
20151114113410
20151116144118
20151118162244
20151201203948
20151203162133
20151203162134
20151209144329
20151209145909
20151210030143
20151210072243
20151210125232
20151210125927
20151210125928
20151210125929
20151210125930
20151210125931
20151210125932
20151218154042
20151221234414
20151224123230
20151228111122
20151228150906
20151228175719
20151229102248
20151229112614
20151230132518
20151231152326
20151231202530
20160106162223
20160106164438
20160109054846
20160113111034
20160118155830
20160118232755
20160119111158
20160119112418
20160119145451
20160120172143
20160121030729
20160122185421
20160128212447
20160128233227
20160129135155
20160129155512
20160202091601
20160202164642
20160204144558
20160209130428
20160212123307
20160217100506
20160217174422
20160220123949
20160222153918
20160223192159
20160225090018
20160225101956
20160226114608
20160227120001
20160227120047
20160229193553
20160301124843
20160302151724
20160302152808
20160305220806
20160307221555
20160308212903
20160309140734
20160310124959
20160310185910
20160314094147
20160314114439
20160314143402
20160315135439
20160316123110
20160316192622
20160316204731
20160317092222
20160320204112
20160324020319
20160328112808
20160328115649
20160328121138
20160329144452
20160331133914
20160331223143
20160407120251
20160412140240
20160412173416
20160412173417
20160412173418
20160413115152
20160415062917
20160415133440
20160416180807
20160416182152
20160419120017
20160419122101
20160421130527
20160425045124
20160504091942
20160504112519
20160508194200
20160508202603
20160508215820
20160508215920
20160508221410
20160509091049
20160509201028
20160516174813
20160516224534
20160518200441
20160519203051
20160522215720
20160525205328
20160527020117
20160528043124
20160530150109
20160603075128
20160603180330
20160603182247
20160608155312
20160608195742
20160608211215
20160610140403
20160610194713
20160610201627
20160610204157
20160610204158
20160610211845
20160610301627
20160614182521
20160615142710
20160615173316
20160615191922
20160616084004
20160616102642
20160616103005
20160616103948
20160617301627
20160620115026
20160628085157
20160629025435
20160703180340
20160705163108
20160712171823
20160713205315
20160715132507
20160715134306
20160715154212
20160715204316
20160715230841
20160716115710
20160718153603
20160721081015
20160722221922
\.


--
-- Data for Name: sent_notifications; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY sent_notifications (id, project_id, noteable_id, noteable_type, recipient_id, commit_id, reply_key, line_code, note_type, "position") FROM stdin;
\.


--
-- Name: sent_notifications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('sent_notifications_id_seq', 1, false);


--
-- Data for Name: services; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY services (id, type, title, project_id, created_at, updated_at, active, properties, template, push_events, issues_events, merge_requests_events, tag_push_events, note_events, build_events, category, "default", wiki_page_events) FROM stdin;
1	AsanaService	\N	1	2016-08-12 15:28:33.175861	2016-08-12 15:28:33.175861	f	{}	f	t	t	t	t	t	t	common	f	t
2	AssemblaService	\N	1	2016-08-12 15:28:33.222555	2016-08-12 15:28:33.222555	f	{}	f	t	t	t	t	t	t	common	f	t
3	BambooService	\N	1	2016-08-12 15:28:33.259354	2016-08-12 15:28:33.259354	f	{}	f	t	t	t	t	t	t	ci	f	t
4	BuildkiteService	\N	1	2016-08-12 15:28:33.287715	2016-08-12 15:28:33.287715	f	{}	f	t	t	t	t	t	t	ci	f	t
5	BuildsEmailService	\N	1	2016-08-12 15:28:33.315487	2016-08-12 15:28:33.315487	f	{"notify_only_broken_builds":true}	f	t	t	t	t	t	t	common	f	t
6	BugzillaService	\N	1	2016-08-12 15:28:33.352616	2016-08-12 15:28:33.352616	f	{}	f	t	t	t	t	t	t	issue_tracker	f	t
7	CampfireService	\N	1	2016-08-12 15:28:33.384656	2016-08-12 15:28:33.384656	f	{}	f	t	t	t	t	t	t	common	f	t
8	CustomIssueTrackerService	\N	1	2016-08-12 15:28:33.413449	2016-08-12 15:28:33.413449	f	{}	f	t	t	t	t	t	t	issue_tracker	f	t
9	DroneCiService	\N	1	2016-08-12 15:28:33.443159	2016-08-12 15:28:33.443159	f	{}	f	t	t	t	t	t	t	ci	f	t
10	EmailsOnPushService	\N	1	2016-08-12 15:28:33.471371	2016-08-12 15:28:33.471371	f	{}	f	t	t	t	t	t	t	common	f	t
11	ExternalWikiService	\N	1	2016-08-12 15:28:33.500525	2016-08-12 15:28:33.500525	f	{}	f	t	t	t	t	t	t	common	f	t
12	FlowdockService	\N	1	2016-08-12 15:28:33.527688	2016-08-12 15:28:33.527688	f	{}	f	t	t	t	t	t	t	common	f	t
13	GemnasiumService	\N	1	2016-08-12 15:28:33.555664	2016-08-12 15:28:33.555664	f	{}	f	t	t	t	t	t	t	common	f	t
14	HipchatService	\N	1	2016-08-12 15:28:33.582292	2016-08-12 15:28:33.582292	f	{"notify_only_broken_builds":true}	f	t	t	t	t	t	t	common	f	t
15	IrkerService	\N	1	2016-08-12 15:28:33.610017	2016-08-12 15:28:33.610017	f	{}	f	t	t	t	t	t	t	common	f	t
16	JiraService	\N	1	2016-08-12 15:28:33.640098	2016-08-12 15:28:33.640098	f	{"api_url":"","jira_issue_transition_id":"2"}	f	t	t	t	t	t	t	issue_tracker	f	t
17	PivotaltrackerService	\N	1	2016-08-12 15:28:33.669515	2016-08-12 15:28:33.669515	f	{}	f	t	t	t	t	t	t	common	f	t
18	PushoverService	\N	1	2016-08-12 15:28:33.697935	2016-08-12 15:28:33.697935	f	{}	f	t	t	t	t	t	t	common	f	t
19	RedmineService	\N	1	2016-08-12 15:28:33.725057	2016-08-12 15:28:33.725057	f	{}	f	t	t	t	t	t	t	issue_tracker	f	t
20	SlackService	\N	1	2016-08-12 15:28:33.758563	2016-08-12 15:28:33.758563	f	{"notify_only_broken_builds":true}	f	t	t	t	t	t	t	common	f	t
21	TeamcityService	\N	1	2016-08-12 15:28:33.790637	2016-08-12 15:28:33.790637	f	{}	f	t	t	t	t	t	t	ci	f	t
22	GitlabIssueTrackerService	\N	1	2016-08-12 15:28:34.356429	2016-08-12 15:28:34.356429	f	{}	f	t	t	t	t	t	t	issue_tracker	t	t
\.


--
-- Name: services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('services_id_seq', 22, true);


--
-- Data for Name: snippets; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY snippets (id, title, content, author_id, project_id, created_at, updated_at, file_name, type, visibility_level) FROM stdin;
\.


--
-- Name: snippets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('snippets_id_seq', 1, false);


--
-- Data for Name: spam_logs; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY spam_logs (id, user_id, source_ip, user_agent, via_api, project_id, noteable_type, title, description, created_at, updated_at) FROM stdin;
\.


--
-- Name: spam_logs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('spam_logs_id_seq', 1, false);


--
-- Data for Name: subscriptions; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY subscriptions (id, user_id, subscribable_id, subscribable_type, subscribed, created_at, updated_at) FROM stdin;
\.


--
-- Name: subscriptions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('subscriptions_id_seq', 1, false);


--
-- Data for Name: taggings; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY taggings (id, tag_id, taggable_id, taggable_type, tagger_id, tagger_type, context, created_at) FROM stdin;
\.


--
-- Name: taggings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('taggings_id_seq', 1, false);


--
-- Data for Name: tags; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY tags (id, name, taggings_count) FROM stdin;
\.


--
-- Name: tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('tags_id_seq', 1, false);


--
-- Data for Name: todos; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY todos (id, user_id, project_id, target_id, target_type, author_id, action, state, created_at, updated_at, note_id, commit_id) FROM stdin;
\.


--
-- Name: todos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('todos_id_seq', 1, false);


--
-- Data for Name: u2f_registrations; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY u2f_registrations (id, certificate, key_handle, public_key, counter, user_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: u2f_registrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('u2f_registrations_id_seq', 1, false);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at, name, admin, projects_limit, skype, linkedin, twitter, authentication_token, theme_id, bio, failed_attempts, locked_at, username, can_create_group, can_create_team, state, color_scheme_id, password_expires_at, created_by_id, last_credential_check_at, avatar, confirmation_token, confirmed_at, confirmation_sent_at, unconfirmed_email, hide_no_ssh_key, website_url, notification_email, hide_no_password, password_automatically_set, location, encrypted_otp_secret, encrypted_otp_secret_iv, encrypted_otp_secret_salt, otp_required_for_login, otp_backup_codes, public_email, dashboard, project_view, consumed_timestep, layout, hide_project_limit, unlock_token, otp_grace_period_started_at, ldap_email, external) FROM stdin;
2	calfuser@capgemini.com	$2a$10$okDLXaC3tCnT/ZHRoFfx1e7JNkp30EAiQtLNTHLaJHeg9.qnoWPxC	\N	\N	\N	26	2016-08-21 11:44:32.130778	2016-08-21 10:23:38.195983	192.168.56.1	192.168.56.1	2016-08-12 14:42:08.96938	2016-08-21 11:44:32.132249	CA LF User	t	10				G8xLENFfK-BXnni_TzYy	2		0	\N	calfuser	t	f	active	1	\N	\N	2016-08-21 11:44:32.064691	\N	\N	2016-08-12 14:42:08.969766	\N	\N	f		calfuser@capgemini.com	f	t		\N	\N	\N	f	\N		0	0	\N	0	f	\N	\N	t	f
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('users_id_seq', 2, true);


--
-- Data for Name: users_star_projects; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY users_star_projects (id, project_id, user_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: users_star_projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('users_star_projects_id_seq', 1, false);


--
-- Data for Name: web_hooks; Type: TABLE DATA; Schema: public; Owner: gitlab
--

COPY web_hooks (id, url, project_id, created_at, updated_at, type, service_id, push_events, issues_events, merge_requests_events, tag_push_events, note_events, enable_ssl_verification, build_events, wiki_page_events, token) FROM stdin;
2	https://pic-calf:3482/jenkins/job/PIC-Project/build	\N	2016-08-12 23:03:19.365803	2016-08-12 23:03:19.365803	SystemHook	\N	t	f	f	f	f	t	f	f	project-pic-token
3	https://pic-calf:3482/jenkins/job/PIC-Project/build	\N	2016-08-12 23:13:44.90546	2016-08-12 23:13:44.90546	SystemHook	\N	t	f	f	f	f	f	f	f	project-pic-token
\.


--
-- Name: web_hooks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gitlab
--

SELECT pg_catalog.setval('web_hooks_id_seq', 4, true);


--
-- Name: abuse_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY abuse_reports
    ADD CONSTRAINT abuse_reports_pkey PRIMARY KEY (id);


--
-- Name: appearances_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY appearances
    ADD CONSTRAINT appearances_pkey PRIMARY KEY (id);


--
-- Name: application_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY application_settings
    ADD CONSTRAINT application_settings_pkey PRIMARY KEY (id);


--
-- Name: audit_events_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY audit_events
    ADD CONSTRAINT audit_events_pkey PRIMARY KEY (id);


--
-- Name: award_emoji_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY award_emoji
    ADD CONSTRAINT award_emoji_pkey PRIMARY KEY (id);


--
-- Name: broadcast_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY broadcast_messages
    ADD CONSTRAINT broadcast_messages_pkey PRIMARY KEY (id);


--
-- Name: ci_application_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_application_settings
    ADD CONSTRAINT ci_application_settings_pkey PRIMARY KEY (id);


--
-- Name: ci_builds_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_builds
    ADD CONSTRAINT ci_builds_pkey PRIMARY KEY (id);


--
-- Name: ci_commits_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_commits
    ADD CONSTRAINT ci_commits_pkey PRIMARY KEY (id);


--
-- Name: ci_events_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_events
    ADD CONSTRAINT ci_events_pkey PRIMARY KEY (id);


--
-- Name: ci_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_jobs
    ADD CONSTRAINT ci_jobs_pkey PRIMARY KEY (id);


--
-- Name: ci_projects_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_projects
    ADD CONSTRAINT ci_projects_pkey PRIMARY KEY (id);


--
-- Name: ci_runner_projects_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_runner_projects
    ADD CONSTRAINT ci_runner_projects_pkey PRIMARY KEY (id);


--
-- Name: ci_runners_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_runners
    ADD CONSTRAINT ci_runners_pkey PRIMARY KEY (id);


--
-- Name: ci_services_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_services
    ADD CONSTRAINT ci_services_pkey PRIMARY KEY (id);


--
-- Name: ci_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_sessions
    ADD CONSTRAINT ci_sessions_pkey PRIMARY KEY (id);


--
-- Name: ci_taggings_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_taggings
    ADD CONSTRAINT ci_taggings_pkey PRIMARY KEY (id);


--
-- Name: ci_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_tags
    ADD CONSTRAINT ci_tags_pkey PRIMARY KEY (id);


--
-- Name: ci_trigger_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_trigger_requests
    ADD CONSTRAINT ci_trigger_requests_pkey PRIMARY KEY (id);


--
-- Name: ci_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_triggers
    ADD CONSTRAINT ci_triggers_pkey PRIMARY KEY (id);


--
-- Name: ci_variables_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_variables
    ADD CONSTRAINT ci_variables_pkey PRIMARY KEY (id);


--
-- Name: ci_web_hooks_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY ci_web_hooks
    ADD CONSTRAINT ci_web_hooks_pkey PRIMARY KEY (id);


--
-- Name: deploy_keys_projects_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY deploy_keys_projects
    ADD CONSTRAINT deploy_keys_projects_pkey PRIMARY KEY (id);


--
-- Name: deployments_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY deployments
    ADD CONSTRAINT deployments_pkey PRIMARY KEY (id);


--
-- Name: emails_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY emails
    ADD CONSTRAINT emails_pkey PRIMARY KEY (id);


--
-- Name: environments_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY environments
    ADD CONSTRAINT environments_pkey PRIMARY KEY (id);


--
-- Name: events_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- Name: forked_project_links_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY forked_project_links
    ADD CONSTRAINT forked_project_links_pkey PRIMARY KEY (id);


--
-- Name: identities_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY identities
    ADD CONSTRAINT identities_pkey PRIMARY KEY (id);


--
-- Name: issues_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY issues
    ADD CONSTRAINT issues_pkey PRIMARY KEY (id);


--
-- Name: keys_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY keys
    ADD CONSTRAINT keys_pkey PRIMARY KEY (id);


--
-- Name: label_links_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY label_links
    ADD CONSTRAINT label_links_pkey PRIMARY KEY (id);


--
-- Name: labels_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY labels
    ADD CONSTRAINT labels_pkey PRIMARY KEY (id);


--
-- Name: lfs_objects_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY lfs_objects
    ADD CONSTRAINT lfs_objects_pkey PRIMARY KEY (id);


--
-- Name: lfs_objects_projects_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY lfs_objects_projects
    ADD CONSTRAINT lfs_objects_projects_pkey PRIMARY KEY (id);


--
-- Name: members_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY members
    ADD CONSTRAINT members_pkey PRIMARY KEY (id);


--
-- Name: merge_request_diffs_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY merge_request_diffs
    ADD CONSTRAINT merge_request_diffs_pkey PRIMARY KEY (id);


--
-- Name: merge_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY merge_requests
    ADD CONSTRAINT merge_requests_pkey PRIMARY KEY (id);


--
-- Name: milestones_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY milestones
    ADD CONSTRAINT milestones_pkey PRIMARY KEY (id);


--
-- Name: namespaces_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY namespaces
    ADD CONSTRAINT namespaces_pkey PRIMARY KEY (id);


--
-- Name: notes_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY notes
    ADD CONSTRAINT notes_pkey PRIMARY KEY (id);


--
-- Name: notification_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY notification_settings
    ADD CONSTRAINT notification_settings_pkey PRIMARY KEY (id);


--
-- Name: oauth_access_grants_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY oauth_access_grants
    ADD CONSTRAINT oauth_access_grants_pkey PRIMARY KEY (id);


--
-- Name: oauth_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY oauth_access_tokens
    ADD CONSTRAINT oauth_access_tokens_pkey PRIMARY KEY (id);


--
-- Name: oauth_applications_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY oauth_applications
    ADD CONSTRAINT oauth_applications_pkey PRIMARY KEY (id);


--
-- Name: personal_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);


--
-- Name: project_group_links_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY project_group_links
    ADD CONSTRAINT project_group_links_pkey PRIMARY KEY (id);


--
-- Name: project_import_data_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY project_import_data
    ADD CONSTRAINT project_import_data_pkey PRIMARY KEY (id);


--
-- Name: projects_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);


--
-- Name: protected_branches_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY protected_branches
    ADD CONSTRAINT protected_branches_pkey PRIMARY KEY (id);


--
-- Name: releases_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY releases
    ADD CONSTRAINT releases_pkey PRIMARY KEY (id);


--
-- Name: sent_notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY sent_notifications
    ADD CONSTRAINT sent_notifications_pkey PRIMARY KEY (id);


--
-- Name: services_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY services
    ADD CONSTRAINT services_pkey PRIMARY KEY (id);


--
-- Name: snippets_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY snippets
    ADD CONSTRAINT snippets_pkey PRIMARY KEY (id);


--
-- Name: spam_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY spam_logs
    ADD CONSTRAINT spam_logs_pkey PRIMARY KEY (id);


--
-- Name: subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY subscriptions
    ADD CONSTRAINT subscriptions_pkey PRIMARY KEY (id);


--
-- Name: taggings_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY taggings
    ADD CONSTRAINT taggings_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: todos_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY todos
    ADD CONSTRAINT todos_pkey PRIMARY KEY (id);


--
-- Name: u2f_registrations_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY u2f_registrations
    ADD CONSTRAINT u2f_registrations_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_star_projects_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY users_star_projects
    ADD CONSTRAINT users_star_projects_pkey PRIMARY KEY (id);


--
-- Name: web_hooks_pkey; Type: CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY web_hooks
    ADD CONSTRAINT web_hooks_pkey PRIMARY KEY (id);


--
-- Name: ci_taggings_idx; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX ci_taggings_idx ON ci_taggings USING btree (tag_id, taggable_id, taggable_type, context, tagger_id, tagger_type);


--
-- Name: index_audit_events_on_author_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_audit_events_on_author_id ON audit_events USING btree (author_id);


--
-- Name: index_audit_events_on_entity_id_and_entity_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_audit_events_on_entity_id_and_entity_type ON audit_events USING btree (entity_id, entity_type);


--
-- Name: index_audit_events_on_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_audit_events_on_type ON audit_events USING btree (type);


--
-- Name: index_award_emoji_on_awardable_type_and_awardable_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_award_emoji_on_awardable_type_and_awardable_id ON award_emoji USING btree (awardable_type, awardable_id);


--
-- Name: index_award_emoji_on_user_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_award_emoji_on_user_id ON award_emoji USING btree (user_id);


--
-- Name: index_award_emoji_on_user_id_and_name; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_award_emoji_on_user_id_and_name ON award_emoji USING btree (user_id, name);


--
-- Name: index_ci_builds_on_commit_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_builds_on_commit_id ON ci_builds USING btree (commit_id);


--
-- Name: index_ci_builds_on_commit_id_and_stage_idx_and_created_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_builds_on_commit_id_and_stage_idx_and_created_at ON ci_builds USING btree (commit_id, stage_idx, created_at);


--
-- Name: index_ci_builds_on_commit_id_and_status_and_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_builds_on_commit_id_and_status_and_type ON ci_builds USING btree (commit_id, status, type);


--
-- Name: index_ci_builds_on_commit_id_and_type_and_name_and_ref; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_builds_on_commit_id_and_type_and_name_and_ref ON ci_builds USING btree (commit_id, type, name, ref);


--
-- Name: index_ci_builds_on_commit_id_and_type_and_ref; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_builds_on_commit_id_and_type_and_ref ON ci_builds USING btree (commit_id, type, ref);


--
-- Name: index_ci_builds_on_erased_by_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_builds_on_erased_by_id ON ci_builds USING btree (erased_by_id);


--
-- Name: index_ci_builds_on_gl_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_builds_on_gl_project_id ON ci_builds USING btree (gl_project_id);


--
-- Name: index_ci_builds_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_builds_on_project_id ON ci_builds USING btree (project_id);


--
-- Name: index_ci_builds_on_project_id_and_commit_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_builds_on_project_id_and_commit_id ON ci_builds USING btree (project_id, commit_id);


--
-- Name: index_ci_builds_on_runner_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_builds_on_runner_id ON ci_builds USING btree (runner_id);


--
-- Name: index_ci_builds_on_status; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_builds_on_status ON ci_builds USING btree (status);


--
-- Name: index_ci_builds_on_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_builds_on_type ON ci_builds USING btree (type);


--
-- Name: index_ci_commits_on_gl_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_commits_on_gl_project_id ON ci_commits USING btree (gl_project_id);


--
-- Name: index_ci_commits_on_gl_project_id_and_sha; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_commits_on_gl_project_id_and_sha ON ci_commits USING btree (gl_project_id, sha);


--
-- Name: index_ci_commits_on_gl_project_id_and_status; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_commits_on_gl_project_id_and_status ON ci_commits USING btree (gl_project_id, status);


--
-- Name: index_ci_commits_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_commits_on_project_id ON ci_commits USING btree (project_id);


--
-- Name: index_ci_commits_on_project_id_and_committed_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_commits_on_project_id_and_committed_at ON ci_commits USING btree (project_id, committed_at);


--
-- Name: index_ci_commits_on_project_id_and_committed_at_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_commits_on_project_id_and_committed_at_and_id ON ci_commits USING btree (project_id, committed_at, id);


--
-- Name: index_ci_commits_on_project_id_and_sha; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_commits_on_project_id_and_sha ON ci_commits USING btree (project_id, sha);


--
-- Name: index_ci_commits_on_sha; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_commits_on_sha ON ci_commits USING btree (sha);


--
-- Name: index_ci_commits_on_status; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_commits_on_status ON ci_commits USING btree (status);


--
-- Name: index_ci_commits_on_user_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_commits_on_user_id ON ci_commits USING btree (user_id);


--
-- Name: index_ci_events_on_created_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_events_on_created_at ON ci_events USING btree (created_at);


--
-- Name: index_ci_events_on_is_admin; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_events_on_is_admin ON ci_events USING btree (is_admin);


--
-- Name: index_ci_events_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_events_on_project_id ON ci_events USING btree (project_id);


--
-- Name: index_ci_jobs_on_deleted_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_jobs_on_deleted_at ON ci_jobs USING btree (deleted_at);


--
-- Name: index_ci_jobs_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_jobs_on_project_id ON ci_jobs USING btree (project_id);


--
-- Name: index_ci_projects_on_gitlab_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_projects_on_gitlab_id ON ci_projects USING btree (gitlab_id);


--
-- Name: index_ci_projects_on_shared_runners_enabled; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_projects_on_shared_runners_enabled ON ci_projects USING btree (shared_runners_enabled);


--
-- Name: index_ci_runner_projects_on_gl_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_runner_projects_on_gl_project_id ON ci_runner_projects USING btree (gl_project_id);


--
-- Name: index_ci_runner_projects_on_runner_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_runner_projects_on_runner_id ON ci_runner_projects USING btree (runner_id);


--
-- Name: index_ci_runners_on_description_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_runners_on_description_trigram ON ci_runners USING gin (description gin_trgm_ops);


--
-- Name: index_ci_runners_on_locked; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_runners_on_locked ON ci_runners USING btree (locked);


--
-- Name: index_ci_runners_on_token; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_runners_on_token ON ci_runners USING btree (token);


--
-- Name: index_ci_runners_on_token_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_runners_on_token_trigram ON ci_runners USING gin (token gin_trgm_ops);


--
-- Name: index_ci_services_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_services_on_project_id ON ci_services USING btree (project_id);


--
-- Name: index_ci_sessions_on_session_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_sessions_on_session_id ON ci_sessions USING btree (session_id);


--
-- Name: index_ci_sessions_on_updated_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_sessions_on_updated_at ON ci_sessions USING btree (updated_at);


--
-- Name: index_ci_taggings_on_taggable_id_and_taggable_type_and_context; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_taggings_on_taggable_id_and_taggable_type_and_context ON ci_taggings USING btree (taggable_id, taggable_type, context);


--
-- Name: index_ci_tags_on_name; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_ci_tags_on_name ON ci_tags USING btree (name);


--
-- Name: index_ci_triggers_on_deleted_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_triggers_on_deleted_at ON ci_triggers USING btree (deleted_at);


--
-- Name: index_ci_triggers_on_gl_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_triggers_on_gl_project_id ON ci_triggers USING btree (gl_project_id);


--
-- Name: index_ci_variables_on_gl_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_ci_variables_on_gl_project_id ON ci_variables USING btree (gl_project_id);


--
-- Name: index_deploy_keys_projects_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_deploy_keys_projects_on_project_id ON deploy_keys_projects USING btree (project_id);


--
-- Name: index_deployments_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_deployments_on_project_id ON deployments USING btree (project_id);


--
-- Name: index_deployments_on_project_id_and_environment_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_deployments_on_project_id_and_environment_id ON deployments USING btree (project_id, environment_id);


--
-- Name: index_deployments_on_project_id_and_environment_id_and_iid; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_deployments_on_project_id_and_environment_id_and_iid ON deployments USING btree (project_id, environment_id, iid);


--
-- Name: index_deployments_on_project_id_and_iid; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_deployments_on_project_id_and_iid ON deployments USING btree (project_id, iid);


--
-- Name: index_emails_on_email; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_emails_on_email ON emails USING btree (email);


--
-- Name: index_emails_on_user_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_emails_on_user_id ON emails USING btree (user_id);


--
-- Name: index_environments_on_project_id_and_name; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_environments_on_project_id_and_name ON environments USING btree (project_id, name);


--
-- Name: index_events_on_action; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_events_on_action ON events USING btree (action);


--
-- Name: index_events_on_author_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_events_on_author_id ON events USING btree (author_id);


--
-- Name: index_events_on_created_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_events_on_created_at ON events USING btree (created_at);


--
-- Name: index_events_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_events_on_project_id ON events USING btree (project_id);


--
-- Name: index_events_on_target_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_events_on_target_id ON events USING btree (target_id);


--
-- Name: index_events_on_target_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_events_on_target_type ON events USING btree (target_type);


--
-- Name: index_forked_project_links_on_forked_to_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_forked_project_links_on_forked_to_project_id ON forked_project_links USING btree (forked_to_project_id);


--
-- Name: index_identities_on_created_at_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_identities_on_created_at_and_id ON identities USING btree (created_at, id);


--
-- Name: index_identities_on_user_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_identities_on_user_id ON identities USING btree (user_id);


--
-- Name: index_issues_on_assignee_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_assignee_id ON issues USING btree (assignee_id);


--
-- Name: index_issues_on_author_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_author_id ON issues USING btree (author_id);


--
-- Name: index_issues_on_confidential; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_confidential ON issues USING btree (confidential);


--
-- Name: index_issues_on_created_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_created_at ON issues USING btree (created_at);


--
-- Name: index_issues_on_created_at_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_created_at_and_id ON issues USING btree (created_at, id);


--
-- Name: index_issues_on_deleted_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_deleted_at ON issues USING btree (deleted_at);


--
-- Name: index_issues_on_description_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_description_trigram ON issues USING gin (description gin_trgm_ops);


--
-- Name: index_issues_on_due_date; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_due_date ON issues USING btree (due_date);


--
-- Name: index_issues_on_milestone_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_milestone_id ON issues USING btree (milestone_id);


--
-- Name: index_issues_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_project_id ON issues USING btree (project_id);


--
-- Name: index_issues_on_project_id_and_iid; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_issues_on_project_id_and_iid ON issues USING btree (project_id, iid);


--
-- Name: index_issues_on_state; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_state ON issues USING btree (state);


--
-- Name: index_issues_on_title; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_title ON issues USING btree (title);


--
-- Name: index_issues_on_title_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_issues_on_title_trigram ON issues USING gin (title gin_trgm_ops);


--
-- Name: index_keys_on_created_at_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_keys_on_created_at_and_id ON keys USING btree (created_at, id);


--
-- Name: index_keys_on_fingerprint; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_keys_on_fingerprint ON keys USING btree (fingerprint);


--
-- Name: index_keys_on_user_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_keys_on_user_id ON keys USING btree (user_id);


--
-- Name: index_label_links_on_label_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_label_links_on_label_id ON label_links USING btree (label_id);


--
-- Name: index_label_links_on_target_id_and_target_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_label_links_on_target_id_and_target_type ON label_links USING btree (target_id, target_type);


--
-- Name: index_labels_on_priority; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_labels_on_priority ON labels USING btree (priority);


--
-- Name: index_labels_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_labels_on_project_id ON labels USING btree (project_id);


--
-- Name: index_lfs_objects_on_oid; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_lfs_objects_on_oid ON lfs_objects USING btree (oid);


--
-- Name: index_lfs_objects_projects_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_lfs_objects_projects_on_project_id ON lfs_objects_projects USING btree (project_id);


--
-- Name: index_members_on_access_level; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_members_on_access_level ON members USING btree (access_level);


--
-- Name: index_members_on_created_at_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_members_on_created_at_and_id ON members USING btree (created_at, id);


--
-- Name: index_members_on_invite_token; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_members_on_invite_token ON members USING btree (invite_token);


--
-- Name: index_members_on_requested_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_members_on_requested_at ON members USING btree (requested_at);


--
-- Name: index_members_on_source_id_and_source_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_members_on_source_id_and_source_type ON members USING btree (source_id, source_type);


--
-- Name: index_members_on_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_members_on_type ON members USING btree (type);


--
-- Name: index_members_on_user_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_members_on_user_id ON members USING btree (user_id);


--
-- Name: index_merge_request_diffs_on_merge_request_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_merge_request_diffs_on_merge_request_id ON merge_request_diffs USING btree (merge_request_id);


--
-- Name: index_merge_requests_on_assignee_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_merge_requests_on_assignee_id ON merge_requests USING btree (assignee_id);


--
-- Name: index_merge_requests_on_author_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_merge_requests_on_author_id ON merge_requests USING btree (author_id);


--
-- Name: index_merge_requests_on_created_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_merge_requests_on_created_at ON merge_requests USING btree (created_at);


--
-- Name: index_merge_requests_on_created_at_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_merge_requests_on_created_at_and_id ON merge_requests USING btree (created_at, id);


--
-- Name: index_merge_requests_on_deleted_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_merge_requests_on_deleted_at ON merge_requests USING btree (deleted_at);


--
-- Name: index_merge_requests_on_description_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_merge_requests_on_description_trigram ON merge_requests USING gin (description gin_trgm_ops);


--
-- Name: index_merge_requests_on_milestone_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_merge_requests_on_milestone_id ON merge_requests USING btree (milestone_id);


--
-- Name: index_merge_requests_on_source_branch; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_merge_requests_on_source_branch ON merge_requests USING btree (source_branch);


--
-- Name: index_merge_requests_on_source_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_merge_requests_on_source_project_id ON merge_requests USING btree (source_project_id);


--
-- Name: index_merge_requests_on_target_branch; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_merge_requests_on_target_branch ON merge_requests USING btree (target_branch);


--
-- Name: index_merge_requests_on_target_project_id_and_iid; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_merge_requests_on_target_project_id_and_iid ON merge_requests USING btree (target_project_id, iid);


--
-- Name: index_merge_requests_on_title; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_merge_requests_on_title ON merge_requests USING btree (title);


--
-- Name: index_merge_requests_on_title_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_merge_requests_on_title_trigram ON merge_requests USING gin (title gin_trgm_ops);


--
-- Name: index_milestones_on_created_at_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_milestones_on_created_at_and_id ON milestones USING btree (created_at, id);


--
-- Name: index_milestones_on_description_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_milestones_on_description_trigram ON milestones USING gin (description gin_trgm_ops);


--
-- Name: index_milestones_on_due_date; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_milestones_on_due_date ON milestones USING btree (due_date);


--
-- Name: index_milestones_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_milestones_on_project_id ON milestones USING btree (project_id);


--
-- Name: index_milestones_on_project_id_and_iid; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_milestones_on_project_id_and_iid ON milestones USING btree (project_id, iid);


--
-- Name: index_milestones_on_title; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_milestones_on_title ON milestones USING btree (title);


--
-- Name: index_milestones_on_title_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_milestones_on_title_trigram ON milestones USING gin (title gin_trgm_ops);


--
-- Name: index_namespaces_on_created_at_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_namespaces_on_created_at_and_id ON namespaces USING btree (created_at, id);


--
-- Name: index_namespaces_on_name; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_namespaces_on_name ON namespaces USING btree (name);


--
-- Name: index_namespaces_on_name_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_namespaces_on_name_trigram ON namespaces USING gin (name gin_trgm_ops);


--
-- Name: index_namespaces_on_owner_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_namespaces_on_owner_id ON namespaces USING btree (owner_id);


--
-- Name: index_namespaces_on_path; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_namespaces_on_path ON namespaces USING btree (path);


--
-- Name: index_namespaces_on_path_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_namespaces_on_path_trigram ON namespaces USING gin (path gin_trgm_ops);


--
-- Name: index_namespaces_on_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_namespaces_on_type ON namespaces USING btree (type);


--
-- Name: index_namespaces_on_visibility_level; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_namespaces_on_visibility_level ON namespaces USING btree (visibility_level);


--
-- Name: index_notes_on_author_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_notes_on_author_id ON notes USING btree (author_id);


--
-- Name: index_notes_on_commit_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_notes_on_commit_id ON notes USING btree (commit_id);


--
-- Name: index_notes_on_created_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_notes_on_created_at ON notes USING btree (created_at);


--
-- Name: index_notes_on_created_at_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_notes_on_created_at_and_id ON notes USING btree (created_at, id);


--
-- Name: index_notes_on_line_code; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_notes_on_line_code ON notes USING btree (line_code);


--
-- Name: index_notes_on_note_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_notes_on_note_trigram ON notes USING gin (note gin_trgm_ops);


--
-- Name: index_notes_on_noteable_id_and_noteable_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_notes_on_noteable_id_and_noteable_type ON notes USING btree (noteable_id, noteable_type);


--
-- Name: index_notes_on_noteable_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_notes_on_noteable_type ON notes USING btree (noteable_type);


--
-- Name: index_notes_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_notes_on_project_id ON notes USING btree (project_id);


--
-- Name: index_notes_on_project_id_and_noteable_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_notes_on_project_id_and_noteable_type ON notes USING btree (project_id, noteable_type);


--
-- Name: index_notes_on_updated_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_notes_on_updated_at ON notes USING btree (updated_at);


--
-- Name: index_notification_settings_on_source_id_and_source_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_notification_settings_on_source_id_and_source_type ON notification_settings USING btree (source_id, source_type);


--
-- Name: index_notification_settings_on_user_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_notification_settings_on_user_id ON notification_settings USING btree (user_id);


--
-- Name: index_notifications_on_user_id_and_source_id_and_source_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_notifications_on_user_id_and_source_id_and_source_type ON notification_settings USING btree (user_id, source_id, source_type);


--
-- Name: index_oauth_access_grants_on_token; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_oauth_access_grants_on_token ON oauth_access_grants USING btree (token);


--
-- Name: index_oauth_access_tokens_on_refresh_token; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_oauth_access_tokens_on_refresh_token ON oauth_access_tokens USING btree (refresh_token);


--
-- Name: index_oauth_access_tokens_on_resource_owner_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_oauth_access_tokens_on_resource_owner_id ON oauth_access_tokens USING btree (resource_owner_id);


--
-- Name: index_oauth_access_tokens_on_token; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_oauth_access_tokens_on_token ON oauth_access_tokens USING btree (token);


--
-- Name: index_oauth_applications_on_owner_id_and_owner_type; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_oauth_applications_on_owner_id_and_owner_type ON oauth_applications USING btree (owner_id, owner_type);


--
-- Name: index_oauth_applications_on_uid; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_oauth_applications_on_uid ON oauth_applications USING btree (uid);


--
-- Name: index_personal_access_tokens_on_token; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_personal_access_tokens_on_token ON personal_access_tokens USING btree (token);


--
-- Name: index_personal_access_tokens_on_user_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_personal_access_tokens_on_user_id ON personal_access_tokens USING btree (user_id);


--
-- Name: index_projects_on_builds_enabled; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_builds_enabled ON projects USING btree (builds_enabled);


--
-- Name: index_projects_on_builds_enabled_and_shared_runners_enabled; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_builds_enabled_and_shared_runners_enabled ON projects USING btree (builds_enabled, shared_runners_enabled);


--
-- Name: index_projects_on_ci_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_ci_id ON projects USING btree (ci_id);


--
-- Name: index_projects_on_created_at_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_created_at_and_id ON projects USING btree (created_at, id);


--
-- Name: index_projects_on_creator_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_creator_id ON projects USING btree (creator_id);


--
-- Name: index_projects_on_description_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_description_trigram ON projects USING gin (description gin_trgm_ops);


--
-- Name: index_projects_on_last_activity_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_last_activity_at ON projects USING btree (last_activity_at);


--
-- Name: index_projects_on_last_repository_check_failed; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_last_repository_check_failed ON projects USING btree (last_repository_check_failed);


--
-- Name: index_projects_on_name_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_name_trigram ON projects USING gin (name gin_trgm_ops);


--
-- Name: index_projects_on_namespace_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_namespace_id ON projects USING btree (namespace_id);


--
-- Name: index_projects_on_path; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_path ON projects USING btree (path);


--
-- Name: index_projects_on_path_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_path_trigram ON projects USING gin (path gin_trgm_ops);


--
-- Name: index_projects_on_pending_delete; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_pending_delete ON projects USING btree (pending_delete);


--
-- Name: index_projects_on_runners_token; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_runners_token ON projects USING btree (runners_token);


--
-- Name: index_projects_on_star_count; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_star_count ON projects USING btree (star_count);


--
-- Name: index_projects_on_visibility_level; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_projects_on_visibility_level ON projects USING btree (visibility_level);


--
-- Name: index_protected_branches_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_protected_branches_on_project_id ON protected_branches USING btree (project_id);


--
-- Name: index_releases_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_releases_on_project_id ON releases USING btree (project_id);


--
-- Name: index_releases_on_project_id_and_tag; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_releases_on_project_id_and_tag ON releases USING btree (project_id, tag);


--
-- Name: index_sent_notifications_on_reply_key; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_sent_notifications_on_reply_key ON sent_notifications USING btree (reply_key);


--
-- Name: index_services_on_category; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_services_on_category ON services USING btree (category);


--
-- Name: index_services_on_created_at_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_services_on_created_at_and_id ON services USING btree (created_at, id);


--
-- Name: index_services_on_default; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_services_on_default ON services USING btree ("default");


--
-- Name: index_services_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_services_on_project_id ON services USING btree (project_id);


--
-- Name: index_services_on_template; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_services_on_template ON services USING btree (template);


--
-- Name: index_snippets_on_author_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_snippets_on_author_id ON snippets USING btree (author_id);


--
-- Name: index_snippets_on_created_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_snippets_on_created_at ON snippets USING btree (created_at);


--
-- Name: index_snippets_on_created_at_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_snippets_on_created_at_and_id ON snippets USING btree (created_at, id);


--
-- Name: index_snippets_on_file_name_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_snippets_on_file_name_trigram ON snippets USING gin (file_name gin_trgm_ops);


--
-- Name: index_snippets_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_snippets_on_project_id ON snippets USING btree (project_id);


--
-- Name: index_snippets_on_title_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_snippets_on_title_trigram ON snippets USING gin (title gin_trgm_ops);


--
-- Name: index_snippets_on_updated_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_snippets_on_updated_at ON snippets USING btree (updated_at);


--
-- Name: index_snippets_on_visibility_level; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_snippets_on_visibility_level ON snippets USING btree (visibility_level);


--
-- Name: index_taggings_on_taggable_id_and_taggable_type_and_context; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_taggings_on_taggable_id_and_taggable_type_and_context ON taggings USING btree (taggable_id, taggable_type, context);


--
-- Name: index_tags_on_name; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_tags_on_name ON tags USING btree (name);


--
-- Name: index_todos_on_author_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_todos_on_author_id ON todos USING btree (author_id);


--
-- Name: index_todos_on_commit_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_todos_on_commit_id ON todos USING btree (commit_id);


--
-- Name: index_todos_on_note_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_todos_on_note_id ON todos USING btree (note_id);


--
-- Name: index_todos_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_todos_on_project_id ON todos USING btree (project_id);


--
-- Name: index_todos_on_state; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_todos_on_state ON todos USING btree (state);


--
-- Name: index_todos_on_target_type_and_target_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_todos_on_target_type_and_target_id ON todos USING btree (target_type, target_id);


--
-- Name: index_todos_on_user_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_todos_on_user_id ON todos USING btree (user_id);


--
-- Name: index_u2f_registrations_on_key_handle; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_u2f_registrations_on_key_handle ON u2f_registrations USING btree (key_handle);


--
-- Name: index_u2f_registrations_on_user_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_u2f_registrations_on_user_id ON u2f_registrations USING btree (user_id);


--
-- Name: index_users_on_admin; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_users_on_admin ON users USING btree (admin);


--
-- Name: index_users_on_authentication_token; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_users_on_authentication_token ON users USING btree (authentication_token);


--
-- Name: index_users_on_confirmation_token; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_users_on_confirmation_token ON users USING btree (confirmation_token);


--
-- Name: index_users_on_created_at_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_users_on_created_at_and_id ON users USING btree (created_at, id);


--
-- Name: index_users_on_current_sign_in_at; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_users_on_current_sign_in_at ON users USING btree (current_sign_in_at);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_email_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_users_on_email_trigram ON users USING gin (email gin_trgm_ops);


--
-- Name: index_users_on_name; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_users_on_name ON users USING btree (name);


--
-- Name: index_users_on_name_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_users_on_name_trigram ON users USING gin (name gin_trgm_ops);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: index_users_on_state; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_users_on_state ON users USING btree (state);


--
-- Name: index_users_on_username; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_users_on_username ON users USING btree (username);


--
-- Name: index_users_on_username_trigram; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_users_on_username_trigram ON users USING gin (username gin_trgm_ops);


--
-- Name: index_users_star_projects_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_users_star_projects_on_project_id ON users_star_projects USING btree (project_id);


--
-- Name: index_users_star_projects_on_user_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_users_star_projects_on_user_id ON users_star_projects USING btree (user_id);


--
-- Name: index_users_star_projects_on_user_id_and_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX index_users_star_projects_on_user_id_and_project_id ON users_star_projects USING btree (user_id, project_id);


--
-- Name: index_web_hooks_on_created_at_and_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_web_hooks_on_created_at_and_id ON web_hooks USING btree (created_at, id);


--
-- Name: index_web_hooks_on_project_id; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE INDEX index_web_hooks_on_project_id ON web_hooks USING btree (project_id);


--
-- Name: subscriptions_user_id_and_ref_fields; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX subscriptions_user_id_and_ref_fields ON subscriptions USING btree (subscribable_id, subscribable_type, user_id);


--
-- Name: taggings_idx; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX taggings_idx ON taggings USING btree (tag_id, taggable_id, taggable_type, context, tagger_id, tagger_type);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: gitlab
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: fk_rails_08903b8f38; Type: FK CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY personal_access_tokens
    ADD CONSTRAINT fk_rails_08903b8f38 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_bfe6a84544; Type: FK CONSTRAINT; Schema: public; Owner: gitlab
--

ALTER TABLE ONLY u2f_registrations
    ADD CONSTRAINT fk_rails_bfe6a84544 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: gitlab
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM gitlab;
GRANT ALL ON SCHEMA public TO gitlab;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

